#ifndef JUGADORGRAF_H
#define JUGADORGRAF_H
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include "jugador.h"

class jugadorgraf:public QObject,
                  public QGraphicsPixmapItem
{
    Q_OBJECT
private:

    jugador *player;
    float escala=1;
    bool flag=1;
    int cont=1;
public:
    jugadorgraf(int personaje=0,bool atras=false,QGraphicsItem* player_ = nullptr);
//    QRectF boundingRect() const;
    void mov(int pers);
    void moverlados(float v_limit);
    void saltar(float v_limit);
    void choquepiso(float v_limit);//choque piso normal
    void choquepiso2(float v_limit);//choque piso cuando coge item de mejora de salto
     void choquepiso3(float v_limit);//choque piso cuando agarra item que impide saltar

    jugador* get_player();
    void posicion(float v_lim);
    ~jugadorgraf();
    float get_escala();

    //estas funciones son para definir el sprites de los jugares para ver a donde miran y para cual jugador elige el usuario
    void eleccion_personaje(int personaje);
    void eleccion_personaje_mov(int personaje);
    void eleccion_personaje_atras(int personaje);
     void eleccion_personajepatder(int personaje);
     void eleccion_personajepatizq(int personaje);
};

#endif // JUGADORGRAF_H
