#include "single.h"
#include "seleccion_personaje.h"
#include "ui_single.h"
#include "mainwindow.h"
#include <QGraphicsView>
#include <QGraphicsScene>
#include <fstream>
#include <pausemenu1.h>

using namespace  std;
single::single(pausemenu1 *pmenu,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::single)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(0,0,1000,500);
    ui->view->setScene(scene);
    ui->view->setBackgroundBrush(QImage(":/imagenes/campo4-bicubic.jpg"));
    ui->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setFixedSize(1000,500);
    contador=0;
    v_name=false;
    v_password=false;
    men=pmenu;
     ui->pushButton_2->setVisible(false);
}

void single::verificar_usuario()
{
    //esta funcion lo que hace es verificaf si el el usiario ya esta registrado o no
       bool estado=false,ingreso=false;
          ifstream archivonombres;
          char letra='k';
          string texto,linea;
          size_t poss=0;
          archivonombres.open("names.txt",ios::in);
          while(archivonombres.good()){
              getline(archivonombres,linea);
              texto="";
              poss=0;
              do{
                  letra=linea[poss];
                  texto+=letra;
                  poss++;
              }while(linea[poss]!=':' and linea!="\0" and !linea.empty());
              if(texto==name){
                  estado=true;
                  break;
              }
              else estado=false;
          }
          //archivonombres.close();
          if(!estado){
              QString nombre=QString::fromUtf8(name.c_str());;
              ofstream archivonombre;
              archivonombre.open("names.txt",ios::app);
              archivonombre<<name<<":"<<password<<'\n';
              ui->label_2->setStyleSheet("color: white;");
              ui->label_2->setText(nombre);
              //ui->label_2->setStyleSheet("color: white;");
              ui->texto ->close();
              ui->pushButton->close();
              ui->label->close();
              ui->label_4->clear();
              ui->label_3->setStyleSheet("color: white;");
              ui->label_3->setText("BIENVENIDO");
              ui->picture->setPixmap(QPixmap(":/imagenes/griezman.png"));
              ui->pushButton_2->setVisible(true);
          }
          else{
              string texto="";
                  poss++;
                  while(linea[poss]!='\0'){
                      texto+=linea[poss];
                      poss++;
                  }
                  if(texto==password){
                      ingreso=true;
                  }
                  else {
                      ingreso=false;
                  }

          }
          v_password=ingreso;
          v_name=estado;
}

single::~single()
{
    delete ui;
}

void single::on_pushButton_clicked()//verifica registro y si la informacion es correcta
{
    QString nombre;
    if(contador==0){
        nombre= ui->texto->text();
        name= nombre.toStdString();
        if(name!="" and name.length()<12){
            ui->label->setStyleSheet("color: white;");
             ui->label->setText("Ingrese la contraseña: ");
        }
        else contador=-1;
        if(name.length()>=12){
            ui->label->setStyleSheet("color: white;");
            ui->label->setText("Ingrese un nombre mas corto: ");
        }
        ui->texto->clear();
    }
    else if(contador==1){
        QString contrasena= ui->texto->text();
        password= contrasena.toStdString();
        contador++;
        ui->pushButton_5->close();
    }
    if(contador==2){
        ui->label->setStyleSheet("color: white;");
        ui->label->setText("Verificando...");
        verificar_usuario();
        if(v_name){
            if(!v_password){
                contador=0;
                //ui->pushButton->setText("Intenta otra vez");
                ui->label->setStyleSheet("color: white;");
                ui->label->setText("Contraseña incorrecta: ");
                ui->texto->clear();
                contador=0;
            }
            else if(v_password){
                nombre=QString::fromUtf8(name.c_str());;
                ui->label_2->setStyleSheet("color: white;");
                ui->label_2->setText(nombre);
                ui->label_3->setStyleSheet("color: white;");
                ui->label_3->setText("BIENVENIDO");
                ui->picture->setPixmap(QPixmap(":/imagenes/griezman.png"));
                ui->pushButton_2->setVisible(true);
                ui->texto ->close();
                ui->pushButton->close();
                ui->pushButton_5->close();
                ui->label->close();
                ui->label_4->clear();
            }
        }
    }
    contador++;



}


void single::on_pushButton_2_clicked()//pasar a la seleccion de personaje
{
    ui->picture->close();
    if(contador==3){
        bool flat=false;
        string linea="",palabra="";
        size_t pos=0;
        ifstream verificardatos;
        verificardatos.open("datossingleplayer.txt",ios::in);
        while(verificardatos.good()){
            getline(verificardatos,linea);
            pos=0;
            palabra="";
            while(!linea.empty() and linea[pos]!=':' and linea[pos]!='\0' ){
                palabra+=linea[pos];
                pos++;
            }
            if(palabra==name){
                flat=true;
                break;
            }
            else flat=false;
        }
        verificardatos.close();
        if(!flat){
            ofstream datos;
            datos.open("datossingleplayer.txt",ios::app);
            datos<<name<<':'<<'0'<<','<<'0'<<','<<'0'<<';'<<"modo un jugador"<<'.'<<'\n';
            datos.close();
        }
        ofstream enjuego;
        enjuego.open("nombreenjuegop1.txt",ios::out);
        enjuego<<name;
        enjuego.close();
        close();
        seleccion_personaje *selec= new seleccion_personaje(men);
        selec->showNormal();
    }
}

void single::on_pushButton_3_clicked()
{
    bool music=false;
    MainWindow *win=new MainWindow(music,men);
    win->show();
    close();

}

void single::on_pushButton_4_clicked()
{
    men->show();
}

void single::on_pushButton_5_clicked()
{
    ui->texto->clear();
}
