#ifndef JUGAR2PLAYER_H
#define JUGAR2PLAYER_H
#include "items.h"
#include <QWidget>
#include <QTimer>
#include <QKeyEvent>
#include <QGraphicsView>
#include <QGraphicsScene>
#include<pausemenu1.h>
#include "jugador.h"
#include "jugadorgrafico.h"
#include <map>
#include <QList>
#include "bola.h"
#include <QTimer>
#include <QMediaPlayer>
#include <QGraphicsItem>

namespace Ui {
class jugar2player;
}

class jugar2player : public QWidget
{
    Q_OBJECT

public:
    explicit jugar2player(pausemenu1 *pmenu=nullptr,QWidget *parent = nullptr);
    void pateo(bool,bool,bool,bola *);
    bool on1,on2,on12,on22,saltando1,saltando2;
    ~jugar2player();

private slots:
    void inicio(bool,bool);
    //relacion teclas  y movimientos
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    //acciones jugador 1(accion1,2 y salta 1)
    //acciones jugador 2 (accion12,22 y salta 2)
    void moverlados();
    void moverlados2();
    void action1(bool);
    void action2(bool);
    void action12(bool);
    void action22(bool);
    void saltar1();
    void saltar2();
    void salta1(bool);
    void salta2(bool);
    void moveteclas();//para que se puedan mover los personajes al tiempo

    void aparecerbolas();
    void animarbola();
    void gol();//definir cuando un jugador marca y poner grito de goolll
    //actualizar datos
    void txtamap();
    void maptotxt();
    void actualizarmap(string name,int jug);

    void time1();
    void poner_items();
    void desaparecer_items();
    void animar_item();
    void reset_efecto();


    void on_pushButton_2_clicked();//menu paussa

private:
    Ui::jugar2player *ui;
    QGraphicsLineItem* l1;
    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;
    QGraphicsLineItem* linea_gol1;
    QGraphicsLineItem* linea_gol2;
    items *item1;
    QMediaPlayer *mgol;
    bool flag=true,empezar,efectoporteria1,efectoporteria2,efectosaltomal1,efectosaltomal2,patear1,patear2,patearh1,patearh2,mirder1,mirder2,mirizq1,mirizq2,i=0;
    double vel1,vel2;
    QTimer *timer_mov1,*timer_mov2,*timer_animaritem,*timer_efecto,*timer_apitem,*timer_desitem,*timergol,*timer_sal1,*timer_sal2,*timer_teclas,*timer_time,* timerbola,*timer_dificultad1,*timer_dificultad2,*timer_par1,*timer_par2;
    jugadorgraf *jug1,*jug2;
    QGraphicsView *view;
    QGraphicsScene *scene;
    int numwin1,numpar1,numwin2,numpar2,case_item,posxitem,efectosal1,efectosal2,efecto,personaje1,personaje2,velypelota1,velxpelota1,velypelota2,velxpelota2,velchoque1,velchoque2,puntaje1,puntaje2,contador_linea,vel, mins,segs1,segs2;
    pausemenu1 *menu;
    map<string,string>datos;
    map<bool,int>teclasmov;
    jugadorgraf *porteria1,*porteria2;
    string namep1,namep2;
    bola *bolajuego;
};

#endif // JUGAR2PLAYER_H
