#include "seleccion_personaje.h"
#include "jugar1player.h"
#include "ui_seleccion_personaje.h"
#include "single.h"
#include <fstream>

using namespace std;
seleccion_personaje::seleccion_personaje(pausemenu1 *pmenu,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::seleccion_personaje)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(0,0,1000,500);
    ui->view->setScene(scene);
    ui->view->setBackgroundBrush(QImage(":/imagenes/jugar1.jpg"));
    ui->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setFixedSize(1000,500);
    eleccion=0;
    contador=0;
    //carga informacion
    txtamap();
    string nam;
    QString nom;
    ifstream enjuego;
    enjuego.open("nombreenjuegop1.txt",ios::in);
    while (enjuego.good()) {
        getline(enjuego,nam);
    }
    nom=QString::fromUtf8(nam.c_str());
    ui->nametag->setStyleSheet("color: white;");
    ui->nametag->setText(nom);
    enjuego.close();
    menu=pmenu;
    QImage image;
    image.load(":/imagenes/Cristianoselc.png");
    ui->personaje->setPixmap(QPixmap::fromImage(image));
}

seleccion_personaje::~seleccion_personaje()
{
    delete ui;

}

void seleccion_personaje::txtamap() //extraer datos del txt con donde se han cargado anterormente
{
    string lineas,nombre="";
    size_t pos=0;
    ifstream archivodatos;
    archivodatos.open("datossingleplayer.txt",ios::in);
    while (archivodatos.good()) {
        getline(archivodatos,lineas);
        pos=0;
        nombre="";
        while(lineas[pos]!='\0' and lineas[pos]!=':' and !lineas.empty()){
            nombre+=lineas[pos];
            pos++;
        }
        if(nombre!=""){
         datos[nombre]=lineas;
        }

    }
    archivodatos.close();
}

void seleccion_personaje::maptotxt()
{
    //actualizar el txt

    ofstream archidato;
    archidato.open("datossingleplayer.txt",ios::out);
    map<string,string>::iterator it;
    for(it=datos.begin();it!=datos.end();it++){
        if(it->second!=""){
             archidato<<datos[it->first]<<'\n';
        }
    }

}



void seleccion_personaje::on_pushButton_clicked()
{
    //actualiza el txt segun el personaje escogido

    string linea,palabra,nombre,aux,name;
    size_t pos=0;
    ifstream doc,enjuego;
    enjuego.open("nombreenjuegop1.txt",ios::in);
    while (enjuego.good()) {
        getline(enjuego,name);
    }
    enjuego.close();
    doc.open("datossingleplayer.txt",ios::in);
    while(doc.good()){
        getline(doc,linea);
        pos=0;
        nombre="";
        while(linea[pos]!='\0' and linea[pos]!=':' and !linea.empty()){
            nombre+=linea[pos];
            pos++;
        }
        if(nombre==name) break;
    }

        while(linea[pos]!='\0' and linea[pos]!=',' and !linea.empty()){
            aux=to_string(contador);
            pos++;
        }
        pos=pos-1;
        linea[pos]=aux[0];

        datos[name]=linea;
        doc.close();
        maptotxt();
        jugar1player *jugar1=new jugar1player(menu);
        jugar1->show();
        close();

}

void seleccion_personaje::on_pushButton_2_clicked()//cambia jugador para seleccionar
{
    QImage image;
    if(contador>0) contador--;
    else contador=3;
    switch (contador) {
    case 0:
        image.load(":/imagenes/Cristianoselc.png");
        ui->personaje->setPixmap(QPixmap::fromImage(image));
        break;
    case 1:
        image.load(":/imagenes/Goenjiselec.png");
        ui->personaje->setPixmap(QPixmap::fromImage(image));
        break;
    case 2:
        image.load(":/imagenes/messiselec.png");
        ui->personaje->setPixmap(QPixmap::fromImage(image));
        break;
    case 3:
        image.load(":/imagenes/Endoselec.png");
        ui->personaje->setPixmap(QPixmap::fromImage(image));
        break;

}

}

void seleccion_personaje::on_pushButton_3_clicked()//cambia jugador para seleccionar
{
    QImage image;
    if(contador<3) contador++;
    else contador=0 ;
    switch (contador) {
    case 0:
        image.load(":/imagenes/Cristianoselc.png");
        ui->personaje->setPixmap(QPixmap::fromImage(image));
        break;
    case 1:
        image.load(":/imagenes/Goenjiselec.png");
        ui->personaje->setPixmap(QPixmap::fromImage(image));
        break;
    case 2:
        image.load(":/imagenes/messiselec.png");
        ui->personaje->setPixmap(QPixmap::fromImage(image));
        break;
    case 3:
        image.load(":/imagenes/Endoselec.png");
        ui->personaje->setPixmap(QPixmap::fromImage(image));
        break;

}
   // eleccion=contador;
}


void seleccion_personaje::on_pushButton_5_clicked()
{
    single *atr=new single(menu);
    atr->show();
    close();
}

void seleccion_personaje::on_pushButton_6_clicked()
{
    menu->showNormal();
}
