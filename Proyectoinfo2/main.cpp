#include "mainwindow.h"
#include <QApplication>
#include <QSplashScreen>
#include <QTimer>
#include <time.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSplashScreen *Splash=new QSplashScreen;
    Splash->setPixmap(QPixmap(":/imagenes/logojuegopng2.png"));
    Splash->show();
    MainWindow w;
    QTimer::singleShot(3000,Splash,SLOT(close()));//mostrar una imagen antes de iniciar el juego
    QTimer::singleShot(3000,&w,SLOT(show()));
    srand(time(NULL));

    return a.exec();
}
