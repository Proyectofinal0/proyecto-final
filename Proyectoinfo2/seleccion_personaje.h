#ifndef SELECCION_PERSONAJE_H
#define SELECCION_PERSONAJE_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <pausemenu1.h>
#include <map>

using namespace std;
namespace Ui {
class seleccion_personaje;
}

class seleccion_personaje : public QWidget,public QGraphicsPixmapItem
{
    Q_OBJECT

public:
    explicit seleccion_personaje(pausemenu1 *pmenu=nullptr,QWidget *parent = nullptr);
    ~seleccion_personaje();
    void txtamap();
    void maptotxt();

private slots:

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

private:
    Ui::seleccion_personaje *ui;
    QGraphicsView *view;
    QGraphicsScene *scene;
    int eleccion,contador;
    map<string,string>datos;
    pausemenu1 *menu;

};

#endif // SELECCION_PERSONAJE_H
