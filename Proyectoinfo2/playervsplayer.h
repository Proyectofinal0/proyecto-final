#ifndef PLAYERVSPLAYER_H
#define PLAYERVSPLAYER_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <pausemenu1.h>
using namespace std;

namespace Ui {
class playervsplayer;
}

class playervsplayer : public QWidget
{
    Q_OBJECT

public:
    explicit playervsplayer(pausemenu1 *pmenu,QWidget *parent = nullptr);
    ~playervsplayer();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();


    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

private:
    Ui::playervsplayer *ui;
    QGraphicsView *view;
    QGraphicsScene *scene;
    int contador1, contador2;
    string passwordp1,passwordp2,name1,name2;
    bool v_passwordp1,v_namep1,v_passwordp2,v_namep2;
    void verificar_usuario(string,string,int);
    pausemenu1 *menu;
};

#endif // PLAYERVSPLAYER_H
