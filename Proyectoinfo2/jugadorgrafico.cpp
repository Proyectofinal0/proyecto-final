#include "jugadorgrafico.h"

jugadorgraf::jugadorgraf(int personaje,bool atras,QGraphicsItem* player_):QGraphicsPixmapItem(player_)
{
    //recibe cual personaje elije el usuario y si mira al frente o atras
    if(!atras){
     eleccion_personaje(personaje);
    }
    else eleccion_personaje_atras(personaje);

    player=new jugador;
}
void jugadorgraf::mov(int eleccion)
{
    if (flag==1 && cont<5)
    {
        eleccion_personaje(eleccion);
        flag=0;
    }
    else if (flag==0&& cont >5)
    {
        eleccion_personaje(eleccion);
        flag=1;
    }
    if (cont==10)cont=0;

    cont++;

}

jugador* jugadorgraf::get_player(){
    return player;
}


void jugadorgraf::posicion(float v_lim){
    setPos(player->get_px(),(v_lim-player->get_py()-((player->get_h())*escala)));
}
void jugadorgraf::moverlados(float v_lim){

    setPos(player->get_px(),(v_lim-player->get_py()-((player->get_h())*escala)));
    player->moverlados();

}
void jugadorgraf::saltar(float v_lim){
     setPos(player->get_px(),(v_lim-player->get_py()-((player->get_h())*escala)));
     player->saltar();
}
void jugadorgraf::choquepiso(float v_lim){
    setPos(player->get_px(),(v_lim-player->get_py()-((player->get_h())*escala)));
    player->choquepiso();
}

void jugadorgraf::choquepiso2(float v_lim)
{
    setPos(player->get_px(),(v_lim-player->get_py()-((player->get_h())*escala)));
    player->choquepiso2();
}

void jugadorgraf::choquepiso3(float v_lim)
{
    setPos(player->get_px(),(v_lim-player->get_py()-((player->get_h())*escala)));
    player->choquepiso3();
}

//void jugadorgraf::choquejug(float v_lim)
//{
//    setPos(player->get_px(),(v_lim-player->get_py()-((player->get_h())*escala)));
//    player->choquejug();
//}



jugadorgraf::~jugadorgraf(){
    delete player;
}
float jugadorgraf::get_escala(){
    return escala;
}

void jugadorgraf::eleccion_personaje(int personaje)
{
    switch (personaje) {
    case 0:
        setPixmap(QPixmap(":/imagenes/Cristianovol.png"));
        break;
    case 1:
        setPixmap(QPixmap(":/imagenes/Goenjivol.png"));
        break;
    case 2:
        setPixmap(QPixmap(":/imagenes/messi vol.png"));
        break;
    case 3:
        setPixmap(QPixmap(":/imagenes/Endo.png"));
        break;
    case 4:
        setPixmap(QPixmap(":/imagenes/dianavol.png"));
        break;
    case 5:
        setPixmap(QPixmap(":/imagenes/dianajul.png"));
        break;
    case 6:
        setPixmap(QPixmap(":/imagenes/dianaarriba.png"));
        break;
    case 7:
        setPixmap(QPixmap(":/imagenes/porteriagrande.png"));
        break;
    case 8:
        setPixmap(QPixmap(":/imagenes/porteriagrandevol.png"));
        break;
    case 9:
        setPixmap(QPixmap(":/imagenes/porteriabiengrande.png"));
        break;
    case 10:
        setPixmap(QPixmap(":/imagenes/porteriabiengrandevol.png"));
        break;
    case 11:
        setPixmap(QPixmap(":/imagenes/porteria.png"));
        break;
    case 12:
        setPixmap(QPixmap(":/imagenes/porteriavol.png"));
        break;

    }
}

void jugadorgraf::eleccion_personaje_mov(int personaje)
{
    switch (personaje) {
    case 0:
        setPixmap(QPixmap(":/imagenes/Cristianovol.png"));
        break;
    case 1:
        setPixmap(QPixmap(":/imagenes/Goenjivol.png"));
        break;
    case 2:
        setPixmap(QPixmap(":/imagenes/messi vol.png"));
        break;
    case 3:
        setPixmap(QPixmap(":/imagenes/Endo.png"));
        break;

    }

}

void jugadorgraf::eleccion_personaje_atras(int personaje)
{

    switch (personaje) {
    case 0:
        setPixmap(QPixmap(":/imagenes/Cristiano-1.png"));
        break;
    case 1:
        setPixmap(QPixmap(":/imagenes/Goenji-1.png"));
        break;
    case 2:
        setPixmap(QPixmap(":/imagenes/Messi.png"));
        break;
    case 3:
        setPixmap(QPixmap(":/imagenes/Endo vol.png"));
        break;

    }


}

void jugadorgraf::eleccion_personajepatder(int personaje)
{
    switch (personaje) {
    case 0:
        setPixmap(QPixmap(":/imagenes/Cristiano pat-1volancho.png"));
        break;
    case 1:
        setPixmap(QPixmap(":/imagenes/Goenji pat-1vol.png"));
        break;
    case 2:
        setPixmap(QPixmap(":/imagenes/Messi pat-1vol.png"));
        break;
    case 3:
        setPixmap(QPixmap(":/imagenes/Endo pat (2).png"));
        break;

    }
}

void jugadorgraf::eleccion_personajepatizq(int personaje)
{
    switch (personaje) {
    case 0:
        setPixmap(QPixmap(":/imagenes/Cristiano pat-1.png"));
        break;
    case 1:
        setPixmap(QPixmap(":/imagenes/Goenji pat-1.png"));
        break;
    case 2:
        setPixmap(QPixmap(":/imagenes/Messi pat-1.png"));
        break;
    case 3:
        setPixmap(QPixmap(":/imagenes/Endo patvol.png"));
        break;

    }
}
