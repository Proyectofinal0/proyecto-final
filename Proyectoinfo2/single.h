#ifndef SINGLE_H
#define SINGLE_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "pausemenu1.h"
#include <QMediaPlayer>
using namespace std;

namespace Ui {
class single;
}

class single : public QWidget
{
    Q_OBJECT

public:
    explicit single(pausemenu1 *pmenu= nullptr,QWidget *parent = nullptr);
    ~single();
        void maptotxt();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::single *ui;
    QGraphicsView *view;
    QGraphicsScene *scene;
    int contador;
    string password,name;
    bool v_password,v_name;
    void verificar_usuario();
    pausemenu1 *men;


};

#endif // SINGLE_H
