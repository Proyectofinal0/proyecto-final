#include "bola.h"
#include <math.h>
#include <tgmath.h>
#include <QBrush>
#include <stdlib.h>
#include "jugadorgrafico.h"
#include "time.h"


bola::bola(jugadorgraf *personaj,jugadorgraf *personaj2,bool ran,jugadorgraf *por1,jugadorgraf *por2,int v1,int v2)
{
    porteria1=por1;
    porteria2=por2;
    personaje=personaj;
    personaje2=personaj2;
    velcho1=v1;
    velcho2=v2;
    Radio =15 /*10+rand()%((10+20)-10)*/;
    Masa = 2000/*1500+rand()%9001*/;
    //si ran es verdadero se generaran balosnes en posicines random y sera el modo 1 jugador
    if(ran){
        PosX = (300+2*Radio)+rand()%(698-(4*Radio));
        PosY = 2*Radio+rand()%(500-(4*Radio));
    }
    else{
        PosX = 500;
        PosY = 250;
    }
    setPos(PosX,PosY);
    //valores iiciales para la fisica de los balones
    VelX=0;// -80+ rand()%161;
    VelY= -80+ rand()%161;


    K = float(10+rand()%50)/100;
    e = float(rand()%51)/10000;
    angulo = atan2(VelY,VelX);
}

QRectF bola::boundingRect() const
{
    return QRectF(0,0,Radio*2,Radio*2);
}

void bola::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPixmap pixmap;
    pixmap.load(":/imagenes/balon.png");
    painter->drawPixmap(boundingRect(),pixmap,pixmap.rect());

}




void bola::move(float dt,bool patea ,int per,bool derecha,bool izq,bool patea2,int per2,bool der2,bool izq2,bool paterh1,bool patearh2 ,int veloy1,int velox1,int veloy2,int velox2,bool veltreecko , int aux)
{
    //la funcion move recibe el dt para calcular las diferentes posiciones,dol booleanos patea y patea2 , para saber si y que jugador esta pateando así como pateah1 y pateah2 para saber si patean de forma horizontal y diferentes enteros que ayudan a modificar la interaccion de los balones con los jugadores segun los items tomados durante el juego
    velpatx1=velox1;
    velpatx2=velox2;
    velpaty1=veloy1;
    velpat2=veloy2;


    magVel2 = (VelX*VelX)+(VelY*VelY);
    AcelX = -(e*(magVel2)*(Radio*Radio)*cos(angulo))/Masa;
    AcelY = -10-(e*(magVel2)*(Radio* Radio)*sin(angulo))/Masa;
    if(veltreecko){
        int auxvelpelota=15;
        velcho1=auxvelpelota;
    }
    else velcho1=aux;


    VelX += AcelX*dt;
    VelY += AcelY*dt;

    //esta parte es para mostrar el sprite del jugador pateando o no segun las banderas que definen la accion
    if(patea || paterh1){
        if(derecha)personaje->eleccion_personajepatder(per);
        else if(izq)personaje->eleccion_personajepatizq(per);
    }
    if(!patea and !paterh1){
        if(derecha)personaje->eleccion_personaje(per);
        else if(izq)personaje->eleccion_personaje_atras(per);
    }
    if(patea2 || patearh2){
        if(der2)personaje2->eleccion_personajepatder(per2);
        else if(izq2)personaje2->eleccion_personajepatizq(per2);
    }
    if(!patea2 and !patearh2){
        if(der2)personaje2->eleccion_personaje(per2);
        else if(izq2)personaje2->eleccion_personaje_atras(per2);
    }


    //se mira si el balon esta colicionando con el balon ,con que jugador lo hace ,si el jugador esta pateando y de que forma esta pateando y así definir que tipo de comportamiento debe tener el balon
    if(collidesWithItem(personaje) and collidesWithItem(personaje2)){
           VelY-=400;
}

    else if(collidesWithItem(personaje) || collidesWithItem(personaje2)/*(PosX>=(personaje->get_player()->get_px()-10) and PosX<=(personaje->get_player()->get_px()+10) */){
        VelY *= -K;
        if(collidesWithItem(personaje)){ //se define si el balon choca atras o delante del jugador por posicion de ambos para así definir ara que direccion debe ir este
                 if(PosX>=personaje->get_player()->get_px() and !patea and !paterh1){
                     VelX=0;
                     if((PosX+(VelX*dt)>=5+(2*Radio)||PosX+(VelX*dt)<(955-(2*Radio)))) {//si el balon choca pero el jugador no patea simplemente se corre un poco
                         PosX+=velcho1;
                         VelX=30;
                     }
                     else PosX+=0;
                 }
                  else if(PosX>=personaje->get_player()->get_px() and patea ){//si el jugador patea de forma parabolica, el balon gana velocidad en x y como el eje esta volteado por qt , se le resta a la velocidad en y para que suba
                     //VelX=100;
                     if((PosX+(VelX*dt)>=5||PosX+(VelX*dt)<=(955-(2*Radio)))){
                         VelX=velox1;
                         VelY=-veloy1;
                         PosX+=0;
                     }
                     personaje->eleccion_personajepatder(per);
                 }
                 else if(PosX>=personaje->get_player()->get_px() and paterh1 ){//si patea de forma horizontal solo se le suma velocidad en x
                    //VelX=100;
                    if((PosX+(VelX*dt)>=5||PosX+(VelX*dt)<=(955-(2*Radio)))){
                        VelX=200;
                    }
                    personaje->eleccion_personajepatder(per);//se muestra el sprite del jugador patando hacia la derecha
                }

                 //lo mismo dearriba pero hacia la izquierda
                 if(PosX<=personaje->get_player()->get_px() and !patea and !paterh1){
                     VelX=0;
                     if((PosX+(VelX*dt)>5+(2*Radio)||PosX+(VelX*dt)<(1000-(2*Radio)))){
                         PosX-=velcho1;
                         VelX=-30;
                     }
                     else PosX-=0;
                 }
                  else if(PosX<=personaje->get_player()->get_px() and patea){
                     //VelX=0;
                     if((PosX+(VelX*dt)>=5||PosX+(VelX*dt)<=(1000-(2*Radio)))){
                         VelX=-velox1;
                         VelY=-veloy1;
                         //PosX=0;
                     }
                     personaje->eleccion_personajepatizq(per);
                 }
                 else if(PosX<=personaje->get_player()->get_px() and paterh1){

                    if((PosX+(VelX*dt)>=5||PosX+(VelX*dt)<=(1000-(2*Radio)))){
                        VelX=-200;
                    }
                    personaje->eleccion_personajepatizq(per);
                }
               }
        //lo mismo del jugador 1 pero con el jugador 2
        else if (collidesWithItem(personaje2)) {
            if(PosX<=(personaje2->get_player()->get_px()+30) and !patea2 and !patearh2){
                VelX=0;
                if((PosX+(VelX*dt)>=20||PosX+(VelX*dt)<=(1000-(2*Radio)))){
                    PosX-=velcho2;
                    VelX=-30;
                }
                else {
                    PosX-=velcho2;
                    VelX=-30;
                }
            }
             else if(PosX<=(personaje2->get_player()->get_px()+30) and patea2){

                if((PosX+(VelX*dt)>=20||PosX+(VelX*dt)<=(1000-(2*Radio)))){
                    VelX=-velox2;
                    VelY=-veloy2;

                }
                personaje2->eleccion_personajepatizq(per2);
            }
            else if(PosX<=(personaje2->get_player()->get_px()+30) and patearh2){

               if((PosX+(VelX*dt)>=50||PosX+(VelX*dt)<=(1000-(2*Radio)))){
                   VelX=-200;
               }
               personaje2->eleccion_personajepatizq(per2);
           }
                    else if(PosX>=(personaje2->get_player()->get_px()) and !patea2 and !patearh2 ){
                        VelX=0;
                        if((PosX+(VelX*dt)>=5||PosX+(VelX*dt)<=(1000-(2*Radio))))
                        {
                            PosX+=velcho2;
                            VelX=30;
                        }
                        else {
                            PosX+=velcho2;
                            VelX=30;
                        }
                    }
                     else if(PosX>=(personaje2->get_player()->get_px()) and patea2 ){

                        if((PosX+(VelX*dt)>=5||PosX+(VelX*dt)<=(1000-(2*Radio)))){
                            VelX=velox2;
                            VelY=-veloy2;
                            PosX+=0;
                        }
                        personaje2->eleccion_personajepatder(per2);
                    }
                    else if(PosX>=(personaje2->get_player()->get_px()) and patearh2 ){

                       if((PosX+(VelX*dt)>=5||PosX+(VelX*dt)<=(1000-(2*Radio)))){
                           VelX=200;
                       }
                       personaje2->eleccion_personajepatder(per2);
                   }

        }
    } //se define el choque del balon con las porterias
    if(collidesWithItem(porteria1)|| collidesWithItem(porteria2)){//se define con que porteria esta chocando
        VelY *= -K;
        if(collidesWithItem(porteria1)) VelX+=5;
        else if(collidesWithItem(porteria2)) VelX-=5;

    }
    //condiciones que se le imponen al balon para su correcto movimiento

    if(PosX+(VelX*dt)<=5||PosX+(VelX*dt)>=(1000-(2*Radio))){
        VelX *= -K;
    }
    if(PosY-(VelY*dt)<=0||PosY-(VelY*dt)>=(485-(2*Radio))){
        VelY *= -K;
    }
    if(abs(VelX)<0.1){
        VelX=0;
    }
    if(abs(VelY)<0.4){
        VelY=0;
    }
    if(abs(VelX*dt + AcelX*(dt*dt)/2)>0.5)
        PosX+=VelX*dt + AcelX*(dt*dt)/2;

    if(abs((VelY*dt + AcelY*(dt*dt)/2))>0.5)
        PosY-=(VelY*dt + AcelY*(dt*dt)/2);

    angulo = atan2(VelY,VelX);
    setPos(PosX,PosY);
}

void bola::set_vxvy(int v1, int v2)
{
    velcho1=v1;
    velcho2=v2;
}

void bola::setporterias(jugadorgraf *porteria11, jugadorgraf *porteria22)
{
    porteria1=porteria11;
    porteria2=porteria22;
}
