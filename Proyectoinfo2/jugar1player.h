#ifndef JUGAR1PLAYER_H
#define JUGAR1PLAYER_H
#include "items.h"
#include <QWidget>
#include <QTimer>
#include <QKeyEvent>
#include <QGraphicsView>
#include <QGraphicsScene>
#include<pausemenu1.h>
#include "jugador.h"
#include "jugadorgrafico.h"
#include <map>
#include <QList>
#include "bola.h"
#include <QTimer>
#include <QGraphicsItem>
#include <QtSerialPort/QSerialPort>
#include <QFileDialog>
#include <QDebug>
#include <QTextStream>
#include <QtSerialPort/QSerialPortInfo>

namespace Ui {
class jugar1player;
}

class jugar1player : public QWidget
{
    Q_OBJECT

public:
    QSerialPort *control;
    explicit jugar1player(pausemenu1 *pmenu=nullptr,QWidget *parent = nullptr);
    ~jugar1player();
    void pateo(bool,bool,bool,bola *);

private slots:
    void inicio(bool w);
    //eventos de las teclas para los movientos
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void moveteclas();//funcion que relaciona los difentes movimientos de los jugadores para poder hacerlos al tiempo
    void saltar();
    void action1(bool);//mover al lado derecho
    void action2(bool);//mover al lado izquierdo
    void action3(bool);//saltar
    void moverlados();
    //actualiza,y carga los valores de las partidas pasadas de los jugadores
    void txtamap();
    void maptotxt();
    void actualizarmap(string name);

   void animarbola();
   void aparecerbolas();
   void destruir();
   void poner_items();
   void desaparecer_items();
   void animar_item();
   void imagnefecto();
   void reset_efecto();
    void time1();//funcion que toma el tiempo de juego
    void rotar_lineas();//va cambiando la posicion de las dianas(objetivos del juego)

    void on_pushButton_2_clicked();//boton de pausa
    void on_pushButton_3_clicked();//boton para introducir com de arduino
    void Joy();//arduino movimientos

    void on_pushButton_clicked();//boton para conectar arduino

private:
    Ui::jugar1player *ui;
    map<string,string>datos;
    map<bool,int>teclasmov;
    items *item1;
    int puntaje,contador_linea,case_item,posxitem,vel,max,numpar,mins,segs1,segs2,velypelota,velxpelota,personaje;
    QGraphicsLineItem* l1;
    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;
    bool flag2=true,patear,patearhorizontal,mirder,mirizq,on1,on2,salta,efectosal,efectosalmal,i=0,efecto,empezar,velitem3,salta2,on11,on21;
    QTimer *timer_mov,*timer_animaritem,*timer_efecto,*timer_picture,*timer_time,*timer_apitem,*timer_desitem,*timer_control,*timer_sal,*timer_par, * timerbola, *timerbola2,* timer_destruir,*timer_lineas,*timer_dificultad1,*timer_dificultad2,*timer_teclas;
    jugadorgraf *jug,*diana;
    QGraphicsView *view;
    QGraphicsScene *scene;
    string namep;
    QString puerto;
    pausemenu1 *menu;
    QList<bola*> bolas;
};

#endif // JUGAR1PLAYER_H
