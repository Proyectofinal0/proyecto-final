
 #include "jugador.h"

jugador::jugador()
{
    px=0;
    py=0;
    h=0;
    w=0;
    vy=50;
    G=10;
    vx=0;
}
void jugador::set_valores(float x, float y, float a, float n)
{
  px=x;
  py=y;
  h=a;
  w=n;
}
float jugador::get_px()
{
  return px;
}
float jugador::get_py()
{
  return py;
}
float jugador::get_vy(){
    return vy;
}
float jugador::get_vx(){
    return vx;
}
float jugador::get_h()
{
  return h;
}
float jugador::get_w()
{
  return w;
}
void jugador::set_px(float x)
{
  px=x;
}
void jugador::set_py(float y)
{
  py=y;
}
void jugador::set_vy(float voy){
    vy=voy;
}
void jugador::set_vx(float vox){
    vx=vox;
}
void jugador::moverlados()
{

        px=px+dt;
        py=py+dt-0.2;

}

void jugador::saltar(){
    py=py+vy*dt+G*dt*dt/2;
    vy=vy-G*dt;
    if (py<0){
        py=0;

    }

}
void jugador::choquepiso()
    {
        py=0;
        vy=50;

}

void jugador::choquepiso2()
{
    py=0;
    vy=70;
}

void jugador::choquepiso3()
{
    py=0;
    vy=00;
}

void jugador::choquejug()
{
    py=60;
    vy=50;
}

