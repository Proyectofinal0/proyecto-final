#ifndef PAUSEMENU1_H
#define PAUSEMENU1_H

#include <QWidget>
#include <QMediaPlayer>
#include <QGraphicsView>
#include <QGraphicsScene>
#include<QTimer>
namespace Ui {
class pausemenu1;
}

class pausemenu1 : public QWidget
{
    Q_OBJECT
private slots:


    void on_play_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_volumenbar_sliderMoved(int position);
    void on_pasarizq_clicked();

    void on_pasarder_clicked();

public:
    explicit pausemenu1(bool a=true,QWidget *parent = nullptr);
        void pausar(QTimer *tiempo,QTimer *apbolas,QTimer *timer3);
    ~pausemenu1();

private:
    Ui::pausemenu1 *ui;
    QGraphicsView *view;
    QGraphicsScene *scene;
    QMediaPlayer *music;
    int contador;
    void seleccionarcancion();
    QTimer *juego,*apabolas,*timer3;
};

#endif // PAUSEMENU1_H
