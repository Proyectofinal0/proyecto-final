#ifndef ITEMS_H
#define ITEMS_H
#include <QObject>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>


class items:public QObject,
        public QGraphicsPixmapItem
{
public:
    items(int sel);
    int get_y();
    int get_per();//funcion que define cual fue el item con el cual se definio
    void actualizar(float dt);//actualiza la posicion
    void choquepiso();//define el choque con el piso para desaparecer los items luego de un tiempo
private:
    int G;
    int posy;
    int vely;
    int selecper;
    void selecpersonaje(int per);//selecciona el tipo de item que va a ser
};

#endif // ITEMS_H
