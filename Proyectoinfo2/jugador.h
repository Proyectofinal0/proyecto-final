#ifndef JUGADOR_H
#define JUGADOR_H


class jugador
{
private:
    float px;
    float py;
    float h=40;
    float w=40;
    float dt=0.1;
    float vy;
    float vx;
    float G;

public:
    jugador();

    void set_valores(float x, float y, float a, float n);
    void moverlados();
    void saltar();
    void choquepiso();//choque de piso normal para reiniciar valores para un nuevo salto
    void choquepiso2();//choque de piso cuando se coge un item que le incrementa el salto al jugador , entonces reinicia valores con un valor superior
    void choquepiso3();//choque con el piso cuando coge un item que impide saltar durante un tiempo
    void choquejug();
    float get_px();

    float get_py();
    float get_vy();
    float get_vx();

    float get_h();

    float get_w();


    void set_px(float x);

    void set_py(float y);
    void set_vy(float vy);
    void set_vx(float vx);
};

#endif // JUGADOR_H
