#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "pausemenu1.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(bool a=true,pausemenu1* menn=nullptr,QWidget *parent = nullptr);
    //en este y todas las otras ventanas el constructor recibe la direccion de memoria del menu de pausa donde se puede modificar la musica de fondo
    ~MainWindow();

private slots:
    //eleccion modo de juego
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();

    void on_actioncerrar_triggered();//cerrar
    //funciones que hacen pocible detectar si el mause esta pasando sobre los pushbuttons y así aparecer unas imagenes
    void buttom1(bool);
    void buttom2(bool);
    void mouse_current_posp();
    void mouse_leavep();
    void mouse_current_posp2();
    void mouse_leavep2();


    void on_actionpause_triggered();

private:
    Ui::MainWindow *ui;
    QGraphicsView *view;
    QGraphicsScene *scene;
    pausemenu1 *music;
};

#endif // MAINWINDOW_H
