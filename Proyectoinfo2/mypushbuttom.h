#ifndef MYPUSHBUTTOM_H
#define MYPUSHBUTTOM_H
#include <QPushButton>
#include <QObject>
#include <QMouseEvent>
#include <QEvent>


class mypushbuttom : public QPushButton
{
    Q_OBJECT
public:
    explicit  mypushbuttom(QWidget *parent=nullptr);
    void mouseMoveEvent(QMouseEvent *ev);
    void leaveEvent(QEvent *);
    int x,y;
signals:

    void Mouse_Posp();
    void Mouse_Leftp();

public slots:
};

#endif // MYPUSHBUTTOM_H
