#include "pausemenu1.h"
#include "ui_pausemenu1.h"
#include <QMediaPlayer>
#include <time.h>

pausemenu1::pausemenu1(bool a,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::pausemenu1)
{

    ui->setupUi(this);
    juego=nullptr;
    apabolas=nullptr;
    scene=new QGraphicsScene(0,0,1000,500);
    ui->view->setScene(scene);
    ui->view->setBackgroundBrush(QImage(":/imagenes/menupau-lanczos3.jpg"));
    ui->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setFixedSize(1000,500);
    contador=0;
    music=new QMediaPlayer(this);
    if(a){//para que la primera vez sea random
        srand(time(NULL));
        contador=0+rand()%((0+5)-0);
        seleccionarcancion();
    }
    else{
        music->stop();
    }
}

pausemenu1::~pausemenu1()
{
    delete ui;
}

void pausemenu1::seleccionarcancion()
{//cambiar la cancion
    switch (contador) {
case 0:
        music->setMedia(QUrl("qrc:/imagenes/live-it-up-official-video-nicky-jam-feat-will-smith-era-istrefi-2018-fifa-world-cup-russia.mp3"));
        music->play();
        break;
case 1:
        music->setMedia(QUrl("qrc:/imagenes/wavin-flag-knaan-ft-david-bisbal-mundial-sudafrica-2010.mp3"));
        music->play();
        break;
case 2:
        music->setMedia(QUrl("qrc:/imagenes/waka-waka-english-ver-with-english-lyrics.mp3"));
        music->play();
        break;
case 4:
        music->setMedia(QUrl("qrc:/imagenes/cali-el-dandee-gol-con-letra-en-pantalla.mp3"));
        music->play();
        break;
case 5:
            music->setMedia(QUrl("qrc:/imagenes/shakira-dare-la-la-la-lyrics.mp3"));
            music->play();
            break;


    }
    if(music->state()==0){
        if(contador<5)contador++;
        else contador=0;
    }
}



void pausemenu1::on_play_clicked()
{
    music->play();
}

void pausemenu1::on_pushButton_2_clicked()
{
    music->pause();
}

void pausemenu1::on_pushButton_clicked()
{
    if(juego!=nullptr and apabolas!=nullptr){
        juego->start();
        apabolas->start();
        timer3->start();
    }
    close();
}

void pausemenu1::on_volumenbar_sliderMoved(int position)
{
       music->setVolume(position);
}


void pausemenu1::on_pasarizq_clicked()
{
    if(contador>0)contador--;
    else contador=5;
    seleccionarcancion();

}

void pausemenu1::on_pasarder_clicked()
{
    if(contador<5)contador++;
    else contador=0;
    seleccionarcancion();
}

void pausemenu1::pausar(QTimer *tiempo,QTimer *apbolas,QTimer *timer31)
{
    timer31->stop();
    tiempo->stop();
    apbolas->stop();
    juego=tiempo;
    apabolas=apbolas;
    timer3=timer31;

}
