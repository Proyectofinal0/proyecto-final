#ifndef JUGAR2PLAYER_H
#define JUGAR2PLAYER_H
#include "items.h"
#include <QWidget>
#include <QTimer>
#include <QKeyEvent>
#include <QGraphicsView>
#include <QGraphicsScene>
#include<pausemenu1.h>
#include "jugador.h"
#include "jugadorgrafico.h"
#include <map>
#include <QList>
#include "bola.h"
#include <QTimer>
#include <QGraphicsItem>

namespace Ui {
class jugar2player;
}

class jugar2player : public QWidget
{
    Q_OBJECT

public:
    explicit jugar2player(pausemenu1 *pmenu=nullptr,QWidget *parent = nullptr);
    void pateo(bool,bool,bool,bola *);
    bool on1,on2,on12,on22,saltando1,saltando2;
    ~jugar2player();

private slots:
    void animarbola();
   void aparecerbolas();
   //void destruirbola();
   void moveteclas();
    void saltar1();
    void saltar2();
    void salta1(bool);
    void salta2(bool);
    void txtamap();
    void maptotxt();
    void actualizarmap(string name,int jug);
    void time1();
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void on_actionGo_triggered();
    void  on_actionStop_triggered();
    void mov();
    void moverlados();
    void action1(bool);
    void action2(bool);
    void inicio(bool,bool);
   // void keyPressEvent(QKeyEvent *event);
    //void keyReleaseEvent(QKeyEvent *event);
    void on_actionGo_triggered2();
     void on_actionStop_triggered2();
    void mov2();
    void moverlados2();
    void action12(bool);
    void action22(bool);
    void poner_items();
    void desaparecer_items();
    void animar_item();
    void reset_efecto();

    void on_pushButton_2_clicked();

private:
    Ui::jugar2player *ui;
    QGraphicsLineItem* l1;
    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;
    QGraphicsLineItem* linea_gol1;
    QGraphicsLineItem* linea_gol2;
    items *item1;
    bool flag=true,empezar,efectoporteria1,efectoporteria2,efectosaltomal1,efectosaltomal2;
    double vel1,vel2;
    bool i=0;
    QTimer *timer_mov1,*timer_mov2,*timer_animaritem,*timer_efecto,*timer_apitem,*timer_desitem;
    QTimer *timer_par1,*timer_par2;
    jugadorgraf *jug1,*jug2;
    QTimer *timer1,*timer2;
    QGraphicsView *view;
    QGraphicsScene *scene;
    int numwin1,numpar1,numwin2,numpar2,case_item,posxitem,efectosal1,efectosal2,efecto;
    int personaje1,personaje2,velypelota1,velxpelota1,velypelota2,velxpelota2,velchoque1,velchoque2;
    pausemenu1 *menu;

    map<string,string>datos;
    map<bool,int>teclasmov;
    int puntaje1,puntaje2,contador_linea;
    bool patear1,patear2,patearh1,patearh2,mirder1,mirder2,mirizq1,mirizq2;
    int vel;
    QTimer *timer_sal1,*timer_sal2,*timer_teclas;
    jugadorgraf *porteria1,*porteria2;
    QTimer *timer_time;
    int mins,segs1,segs2;
    string namep1,namep2;
    QTimer * timerbola,*timer_dificultad1,*timer_dificultad2;
    bola *bolajuego;
};

#endif // JUGAR2PLAYER_H
