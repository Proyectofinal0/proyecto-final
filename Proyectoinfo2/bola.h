#ifndef BOLA_H
#define BOLA_H

#include <QGraphicsEllipseItem>
#include <QGraphicsItem>
#include "jugadorgrafico.h"
#include <QPainter>
#include <iostream>


using namespace std;


class bola: public QObject,
        public QGraphicsItem

{

public:
    bola(jugadorgraf *personaj,jugadorgraf *personaj2=nullptr,bool ran=true,jugadorgraf *porteria1=nullptr,jugadorgraf *porteria2=nullptr,int velchoque1=5,int velchoque2=5);
    //el constructor de bola recibe  la direccion de emoria de los jugadores,porterias,un boleano ran que define si se quiere que aparescan los balones en posiciones aleatoreas o en una sola, y dos enteros que son las velocidades predeterminadas que tendran los balones al chocar con los balones
    QRectF boundingRect() const;
    void paint(QPainter *painter,
    const QStyleOptionGraphicsItem *option, QWidget *widget);
    void move(float dt,bool patea,int per,bool der,bool izq,bool patea2=false,int per2=0,bool der2=false,bool izq2=false,bool paterh1=false,bool paterh2=false,int veloy1=400,int velox1=100,int veloy2=400,int velox2=100,bool veltreecko=false,int aux=5);
    void set_vxvy(int v1, int v2); //funcion que ayuda a pasar le nuevas velocidades tomadas por los balones cuando son pateadas por el jugador segun los items
    void setporterias(jugadorgraf *porteria1=nullptr,jugadorgraf *porteria2=nullptr);//funcion que pasa las direcciones de memoria de las porterias en el modo multiplayer para asi definir los choques con ellas
private:
    float VelX, VelY, K, e, angulo, AcelX, AcelY, magVel2;
    int Masa, Radio, PosX, PosY,velcho1,velcho2,velpatx1,velpatx2,velpaty1,velpat2;
    jugadorgraf *personaje,*personaje2,*porteria1,*porteria2;//variables locales donde se guardaran la direccion de memoria de los jugadores y porterias para definir las fisicas


};

#endif // BOLA_H
