#include "jugar2player.h"
#include "ui_jugar2player.h"
#include "mainwindow.h"
#include "seleccion_multiplayer.h"
#include <fstream>
#include <QMessageBox>

using namespace std;
jugar2player::jugar2player(pausemenu1 *pmenu,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::jugar2player)
{
    ui->setupUi(this);
    txtamap();
    scene=new QGraphicsScene(0,0,1000,500);
    ui->view->setScene(scene);
    ui->view->setBackgroundBrush(QImage(":/imagenes/campo2.jpg"));
    efectosal1=0;
    efectosal2=0;
    empezar=0;
    efecto=0;
    efectosaltomal1=0;
    efectosaltomal2=0;
    velxpelota1=72;
    velypelota1=250;
    velxpelota2=72;
    velypelota2=250;
    puntaje1=0;
    puntaje2=0;
    efectoporteria1=0;
    efectoporteria2=0;
    mins=0;
    segs1=0;
    segs2=0;
    item1=nullptr;
    contador_linea=0;
    patear1=false;
    patear2=false;
    menu=pmenu;
    timer_efecto=new QTimer(this);
    timer_desitem=new QTimer(this);
    timer_animaritem=new QTimer(this);
    timer_apitem=new QTimer(this);
    timer_teclas= new QTimer(this);
    timer_dificultad1= new QTimer(this);
    timer_dificultad2= new QTimer(this);
    timer_sal1=new QTimer(this);
    timer_sal2=new QTimer(this);
    timer_time=new QTimer(this);
    timer_time->stop();
    timer_sal1->stop();
    timer_sal2->stop();
    timer_dificultad1->stop();
    timer_dificultad2->stop();
    timer_teclas->stop();
    timer_apitem->stop();
    timer_animaritem->stop();
    timer_desitem->stop();
    timer_efecto->stop();
    mirder1=true;
    mirizq1=false;
    mirder2=false;
    mirizq2=true;
    velchoque1=5;
    velchoque2=5;
    ui->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setFixedSize(1000,500);
    linea_gol1=new QGraphicsLineItem(40,370,40,500);
    linea_gol1->setVisible(false);
    scene->addItem(linea_gol1);
    porteria1=new jugadorgraf(7);
    porteria1->setPos(0,340);
    scene->addItem(porteria1);
    linea_gol2=new QGraphicsLineItem(962,370,962,500);
    linea_gol2->setVisible(false);
    scene->addItem(linea_gol2);
    porteria2=new jugadorgraf(8);
    porteria2->setPos(920,340);
    scene->addItem(porteria2);

    numwin1=0;
    numpar1=0;
    numwin2=0;
    numpar2=0;
    on1=0;
    on2=0;
    on12=0;
    on22=0;
    saltando1=1;
    saltando2=1;
    timer_mov1 = new QTimer(this);
    timer_par1 = new QTimer(this);
    timer1=new QTimer();
    timer_mov1->stop();
    timer_par1->stop();
    timer_mov2 = new QTimer(this);
    timer_par2 = new QTimer(this);
    timer2=new QTimer();
    timer_mov2->stop();
    timer_par2->stop();
    timerbola = new QTimer;
    //aparecerbolas();
    connect(timer_efecto,SIGNAL(timeout()),this,SLOT(reset_efecto()));
    connect(timer_desitem,SIGNAL(timeout()),this,SLOT(desaparecer_items()));
    connect(timer_animaritem,SIGNAL(timeout()),this,SLOT(animar_item()));
    connect(timer_apitem,SIGNAL(timeout()),this,SLOT(poner_items()));
    connect(timer_teclas,SIGNAL(timeout()),this,SLOT(moveteclas()));
    connect(timerbola,SIGNAL(timeout()),this,SLOT(animarbola()));
    connect(timer_mov1,SIGNAL(timeout()),this,SLOT(mov()));
    connect(timer_par1,SIGNAL(timeout()),this,SLOT(moverlados()));
   // connect(timer1,SIGNAL(timeout()),this,SLOT(animar()));
    connect(timer_mov2,SIGNAL(timeout()),this,SLOT(mov2()));
    connect(timer_par2,SIGNAL(timeout()),this,SLOT(moverlados2()));
    connect(timer_sal1,SIGNAL(timeout()),this,SLOT(saltar1()));
    connect(timer_sal2,SIGNAL(timeout()),this,SLOT(saltar2()));
    connect(timer_time,SIGNAL(timeout()),this,SLOT(time1()));
    timer_teclas->start(20);
    timerbola->start(20);
    timer_apitem->start(15000);
    timer_animaritem->start(20);
    l1=new QGraphicsLineItem(0,0,1000,0);
    l2=new QGraphicsLineItem(0,0,0,500);
    l3=new QGraphicsLineItem(1000,0,1000,500);
    l4=new QGraphicsLineItem(0,500,1000,500,nullptr);
    scene->addItem(l1);
    scene->addItem(l2);
    scene->addItem(l3);
    scene->addItem(l4);
    vel1=5;
    vel2=5;
    i=0;
    ifstream llenardata,namejuego;
        menu=pmenu;
    namejuego.open("nombresenjuego-pvsp.txt",ios::in);
        string linea,nombregood1,nombregood2,nameveri,texto;
        QString name1,name2,scoreq,partidosq,punta1,punta2;
        size_t pos=0;
    while (namejuego.good()) {
        getline(namejuego,nombregood1);
        getline(namejuego,nombregood2);
    }
    namep1=nombregood2;
    namep2=nombregood1;
    name1=QString::fromUtf8(nombregood1.c_str());
    name2=QString::fromUtf8(nombregood2.c_str());
    ui->nametag1->setStyleSheet("color: white;");
    ui->nametag2->setStyleSheet("color: white;");
    ui->nametag1->setText(name1);
    ui->nametag2->setText(name2);
    ui->nametag1_2->setStyleSheet("color: white;");
    ui->nametag2_2->setStyleSheet("color: white;");
    ui->nametag1_2->setText(name1);
    ui->nametag2_2->setText(name2);
    llenardata.open("datosmultiplayer.txt",ios::in);
    while(llenardata.good()){
        getline(llenardata,linea);
        pos=0;
        texto="";
        nameveri="";
        while(linea[pos]!='\0' and linea[pos]!=':' and !linea.empty()){
            nameveri+=linea[pos];
            pos++;
        }
        if(nameveri==nombregood1){
            pos++;
            texto="";
            while(linea[pos]!='\0' and linea[pos]!=',' and !linea.empty()){
                texto+=linea[pos];
                pos++;
            }
            personaje1=atoi(texto.c_str());
            pos++;
            texto="";
            while(linea[pos]!='\0' and linea[pos]!=',' and !linea.empty()){
                texto+=linea[pos];
                pos++;
            }
            partidosq=QString::fromUtf8(texto.c_str());
            ui->numpar1->setStyleSheet("color: white;");
            ui->numpar1->setText(partidosq);
            numpar1=atoi(texto.c_str());
            texto="";
            pos++;
            while(linea[pos]!='\0' and linea[pos]!=';' and !linea.empty()){
                texto+=linea[pos];
                pos++;
            }
            numwin1=atoi(texto.c_str());
            scoreq=QString::fromUtf8(texto.c_str());
            ui->ganados1->setStyleSheet("color: white;");
            ui->ganados1->setText(scoreq);
        }
        else if(nameveri==nombregood2){
            texto="";
            pos++;

            while(linea[pos]!='\0' and linea[pos]!=',' and !linea.empty()){
                texto+=linea[pos];
                pos++;
            }
            personaje2=atoi(texto.c_str());
            pos++;
            texto="";
            while(linea[pos]!='\0' and linea[pos]!=',' and !linea.empty()){
                texto+=linea[pos];
                pos++;
            }
            partidosq=QString::fromUtf8(texto.c_str());
            ui->numpar2->setStyleSheet("color: white;");
            ui->numpar2->setText(partidosq);
            numpar2=atoi(texto.c_str());
            texto="";
            pos++;
            while(linea[pos]!='\0' and linea[pos]!=';' and !linea.empty()){
                texto+=linea[pos];
                pos++;
            }
            numwin2=atoi(texto.c_str());
            scoreq=QString::fromUtf8(texto.c_str());
            ui->ganados2->setStyleSheet("color: white;");
            ui->ganados2->setText(scoreq);
        }
    }
    punta1=QString::fromUtf8(to_string(puntaje1).c_str());
    punta2=QString::fromUtf8(to_string(puntaje2).c_str());
    ui->score1->setStyleSheet("color:white;");
    ui->score2->setStyleSheet("color:white;");
    ui->score1->display(punta1);
    ui->score2->display(punta2);
    jug1=new jugadorgraf(personaje1);
    jug1->get_player()->set_valores(200,0,100,50);
    jug1->posicion(500);
    scene->addItem(jug1);
    scene->setFocusItem(jug1);
    bool atras=true;
    jug2=new jugadorgraf(personaje2,atras);
    jug2->get_player()->set_valores(700,0,100,50);
    jug2->posicion(500);
    scene->addItem(jug2);
    scene->setFocusItem(jug2);
    aparecerbolas();
    llenardata.close();
}
void jugar2player::txtamap()
{
    string lineas,nombre="";
    size_t pos=0;
    ifstream archivodatos;
    archivodatos.open("datosmultiplayer.txt",ios::in);
    while (archivodatos.good()) {
        getline(archivodatos,lineas);
        pos=0;
        nombre="";
        while(lineas[pos]!='\0' and lineas[pos]!=':' and !lineas.empty()){
            nombre+=lineas[pos];
            pos++;
        }
        if(nombre!=""){
         datos[nombre]=lineas;
        }

    }
    archivodatos.close();
}

void jugar2player::maptotxt()
{
    ofstream archidato;
    archidato.open("datosmultiplayer.txt",ios::out);
    map<string,string>::iterator it;
    for(it=datos.begin();it!=datos.end();it++){
        if(it->second!=""){
             archidato<<datos[it->first]<<'\n';
        }
    }
    archidato.close();
}

void jugar2player::actualizarmap(string name,int jug)
{
    if(jug==1){
        string numpersonaje=to_string(personaje1),numwinn1=to_string(numwin1),numpart=to_string(numpar1);
        string newline=name+":"+numpersonaje+","+numpart+","+numwinn1+";"+"modo player vs player.";
        datos[name]=newline;
    }
    else if(jug==2){
        string numpersonaje2=to_string(personaje2),numwinn2=to_string(numwin2),numpart2=to_string(numpar2);
        string newline2=name+":"+numpersonaje2+","+numpart2+","+numwinn2+";"+"modo player vs player.";
        datos[name]=newline2;
    }
}
void jugar2player::aparecerbolas()
{
    bolajuego = new bola(jug1,jug2,false,porteria1,porteria2,velchoque1,velchoque2);
    scene->addItem(bolajuego);
    scene->setFocusItem(bolajuego);
}

void jugar2player::moveteclas()
{
    action1(teclasmov[on2]);
    action12(teclasmov[on22]);
    action2(teclasmov[on1]);
    action22(teclasmov[on12]);
    salta1(teclasmov[saltando1]);
    salta2(teclasmov[saltando2]);
    inicio(teclasmov[saltando1],teclasmov[saltando2]);
}
void jugar2player::animarbola()
{
        bolajuego->move(0.1,patear1,personaje1,mirder1,mirizq1,patear2,personaje2,mirder2,mirizq2,patearh1,patearh2,velypelota1,velxpelota1,velypelota2,velxpelota2);
        if(!(bolajuego->collidingItems().empty()))
        {if(bolajuego->collidesWithItem(linea_gol1)){
            scene->removeItem(bolajuego);
            bolajuego->~bola();
            aparecerbolas();
            puntaje1+=1;
            string punta1=to_string(puntaje1);
            QString npuntaje=QString::fromUtf8(punta1.c_str());
            ui->score2->setStyleSheet("color:white;");
            ui->score2->display(npuntaje);
        }
        else if(bolajuego->collidesWithItem(linea_gol2)){
                scene->removeItem(bolajuego);
                bolajuego->~bola();
                aparecerbolas();
                puntaje2+=1;
                string punta2=to_string(puntaje2);
                QString npuntaje2=QString::fromUtf8(punta2.c_str());
                ui->score1->setStyleSheet("color:white;");
                 ui->score1->display(npuntaje2);
            }
        }


}
void jugar2player::time1()
{
    segs2+=1;
    if(segs2==10){
        segs1+=1;
        segs2=0;
        if(segs1==6) {
            mins+=1;
            segs1=0;
        }
    }
    QString mensaje,auxloser;
    string mns=to_string(mins),sgs1=to_string(segs1),sgs2=to_string(segs2);
    QString mn=QString::fromUtf8(mns.c_str()),sg1=QString::fromUtf8(sgs1.c_str()),sg2=QString::fromUtf8(sgs2.c_str());
    ui->seg->setStyleSheet("color:white;");
    ui->seg_2->setStyleSheet("color:white;");
    ui->min->setStyleSheet("color:white;");
    ui->seg->setText(sg1);
    ui->seg_2->setText(sg2);
    ui->min->setText(mn);
    if(mins==1){
        if(puntaje1>puntaje2){
            const string text="Fin del juego,El Ganador es  "+namep1;
           mensaje=QString::fromUtf8(text.c_str());
           numwin1+=1;
           if(puntaje1-puntaje2>=3){
               const string ls=" , "+namep2+"  es un loserrrr";
               auxloser=QString::fromUtf8(ls.c_str());
               mensaje+=auxloser;
           }
        }
        else if (puntaje1<puntaje2){
            const string text="Fin del juego,El Ganador es  "+namep2;
             mensaje=QString::fromUtf8(text.c_str());
             numwin2+=1;
             if(puntaje2-puntaje1>=3){
                 const string ls=" , "+namep1+"  es un loserrrr";
                 auxloser=QString::fromUtf8(ls.c_str());
                 mensaje+=auxloser;
             }
        }
        else if (puntaje1==puntaje2){
            const string text="Fin del juego,""Empate,  "+namep1+" y "+namep2+"  Son dos ABURRIDOS";
             mensaje=QString::fromUtf8(text.c_str());
        }
        timer_time->stop();
        QMessageBox::information(this,"Fin del juego",mensaje);
        QMessageBox::StandardButton ok=QMessageBox::question(this,"Fin del juego","¿Quieres seguir jugando?",QMessageBox::Yes | QMessageBox::No);
        if(ok==QMessageBox::Yes){
            numpar1+=1;
            numpar2+=1;
            actualizarmap(namep1,1);
            actualizarmap(namep2,2);
            maptotxt();
            seleccion_multiplayer *win1=new seleccion_multiplayer(menu);
            win1->showNormal();
            close();
        }
        else if(ok==QMessageBox::No){
            numpar1+=1;
            numpar2+=1;
            actualizarmap(namep1,1);
            actualizarmap(namep2,2);
            maptotxt();
            bool music=false;
            MainWindow *win=new MainWindow(music,menu);
            win->show();
            close();

        }
    }
}
void jugar2player::saltar1(){
jug1->saltar(500);
}
void jugar2player::saltar2(){
    jug2->saltar(500);
}

void jugar2player::salta1(bool on)
{
    if(on){
        timer_sal1->stop();
        timer_sal1->start(10);
        jug1->get_player()->set_py(jug1->get_player()->get_py());

    if (jug1->get_player()->get_py()<=0.5 && jug1->collidesWithItem(l4)){
        if(!efectosal1 and !efectoporteria1){
           jug1->choquepiso(0);
        }
        else if(efectosal1) jug1->choquepiso2(0);
        else if(efectosaltomal1) jug1->choquepiso3(0);
    }

    }

}

void jugar2player::salta2(bool on)
{
    if(on){
        timer_sal2->stop();
        timer_sal2->start(10);
        jug2->get_player()->set_py(jug2->get_player()->get_py());

    if (jug2->get_player()->get_py()<=0.5 && jug2->collidesWithItem(l4)){
        if(!efectosal2 and !efectosaltomal2){
           jug2->choquepiso(0);
        }
        else if(efectosal2) jug2->choquepiso2(0);
        else if(efectosaltomal2) jug2->choquepiso3(0);
    }

    }

}

void jugar2player::on_actionGo_triggered()
    {
    timer_mov1->start(50);
    }
void jugar2player::on_actionGo_triggered2()
{
timer_mov2->start(50);
}

void jugar2player::mov()
{
//jug1->mov1(personaje);
}
void jugar2player::mov2()
{
//jug2->mov2(personaje);
}
void jugar2player::moverlados()
{

if (i==0){
jug1->get_player()->set_px(0);
jug1->get_player()->set_py(200);
i=1;
}
if (jug1->get_player()->get_py()>0) jug1->moverlados(500);
}
void jugar2player::moverlados2()
{

if (i==0){
jug2->get_player()->set_px(0);
jug2->get_player()->set_py(200);
i=1;
}
if (jug2->get_player()->get_py()>0) jug2->moverlados(500);
}

void jugar2player::action1(bool on)
{

    if(on && jug1->get_player()->get_px()<960-(jug1->get_player()->get_w()*jug1->get_escala())){
        jug1->get_player()->set_px(jug1->get_player()->get_px()+vel1);
        on_actionStop_triggered();
    }
}
void jugar2player::action2(bool on)
{

     if(on && jug1->get_player()->get_px()>50 ){
        jug1->get_player()->set_px(jug1->get_player()->get_px()-vel1) ;
        on_actionStop_triggered();
     }
}

void jugar2player::inicio(bool w, bool ocho)
{
    if(w and ocho and empezar==0){
        empezar=1;
        ui->empezar->close();
        ui->mov2->close();
        ui->mov1->close();
        timer_time->start(1000);
    }
}
void jugar2player::action12(bool on)
{
     if(on && jug2->get_player()->get_px()<950-(jug2->get_player()->get_w()*jug2->get_escala())){
        jug2->get_player()->set_px(jug2->get_player()->get_px()+vel2);
        on_actionStop_triggered2();
    }

}
void jugar2player::action22(bool on )
{
    if(on &&  jug2->get_player()->get_px()>50 ){
        jug2->get_player()->set_px(jug2->get_player()->get_px()-vel2) ;
        on_actionStop_triggered2();
    }
}

void jugar2player::on_actionStop_triggered()
{
    timer_mov1->stop();
    timer_par1->stop();
    i=0;
}
void jugar2player::on_actionStop_triggered2()
{
    timer_mov2->stop();
    timer_par2->stop();
    i=0;
}

void jugar2player::keyPressEvent(QKeyEvent *event){       //Funciones de teclas
    if(event->key()==Qt::Key_F4) close();
    if(event->key()== Qt::Key_D&& jug1->get_player()->get_px()<960-(jug1->get_player()->get_w()*jug1->get_escala())  )
    {
        mirder1=true;
        mirizq1=false;
        teclasmov[on1]=1;
       on1=teclasmov[on1];
//        jug->get_player()->set_px(jug->get_player()->get_px()+vel);
//        on_actionStop_triggered();
        jug1->eleccion_personaje(personaje1);
        event->accept();
    }
   if(event->key()== Qt::Key_A&& jug1->get_player()->get_px()>50)
    {
       mirder1=false;
       mirizq1=true;
       teclasmov[on2]=1;
       on2=true;
//        jug->get_player()->set_px(jug->get_player()->get_px()-vel) ;
//        on_actionStop_triggered();
       jug1->eleccion_personaje_atras(personaje1);
        event->accept();
    }
   if(event->key()==Qt::Key_H){
       patear1=true;
   }
   if(event->key()==Qt::Key_J){
       patearh1=true;
   }
    if(event->key()== Qt::Key_W )
    {
        teclasmov[saltando1]=0;
        saltando1=false;
            event->accept();
    }
   if((event->key()==Qt::Key_6&& jug2->get_player()->get_px()<950-(jug2->get_player()->get_w()*jug2->get_escala()) )){
       mirder2=true;
       mirizq2=false;
       teclasmov[on12]=1;
      on12=true;
       jug2->eleccion_personaje(personaje2);
       event->accept();
   }
   if(event->key()==Qt::Key_4&& jug2->get_player()->get_px()>50 ){
       mirder2=false;
       mirizq2=true;
       teclasmov[on22]=1;
       on22=true;
       jug2->eleccion_personaje_atras(personaje2);
        event->accept();
   }
   if(event->key()== Qt::Key_8)
   {
      teclasmov[saltando2]=0;
      saltando2=false;
           event->accept();
   }
   if(event->key()==Qt::Key_9){
       patear2=true;
   }
   if(event->key()==Qt::Key_0){
       patearh2=true;
   }
//jug->mov(personaje);
jug1->posicion(500);
jug2->posicion(500);
}

void jugar2player::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key()== Qt::Key_D )
    {
        teclasmov[on1]=0;
       on1=false;
//        jug->get_player()->set_px(jug->get_player()->get_px()+vel);
//        on_actionStop_triggered();
        event->accept();
    }
    if(event->key()== Qt::Key_A)
     {
        teclasmov[on2]=0;
        on2=false;
 //        jug->get_player()->set_px(jug->get_player()->get_px()-vel) ;
 //        on_actionStop_triggered();
        //jug->eleccion_personaje(personaje);
         event->accept();
     }
    if(event->key()==Qt::Key_H){
        patear1=false;
    }
    if(event->key()==Qt::Key_J){
        patearh1=false;
    }
    if(event->key()==Qt::Key_W){
        teclasmov[saltando1]=1;
        saltando1=1;
        event->accept();
    }
    if(event->key()== Qt::Key_6)
    {
        teclasmov[on12]=0;
        on12=false;
        event->accept();
    }
    if(event->key()== Qt::Key_4 )
     {
        teclasmov[on22]=0;
        on22=false;
        //jug->eleccion_personaje(personaje);
         event->accept();
     }
    if(event->key()==Qt::Key_8){
        teclasmov[saltando2]=1;
        saltando2=1;
        event->accept();
    }
    if(event->key()==Qt::Key_9){
        patear2=false;
    }
    if(event->key()==Qt::Key_0){
        patearh2=false;
    }

}
void jugar2player::poner_items()
{
    int personaje,posy,posx;
    posy=10;
    //srand(time(NULL));
    posx=100+rand()%((100+800)-100);
    posxitem=posx;
    personaje=0+rand()%((0+7)-0);
    item1=new items(personaje);
    scene->addItem(item1);
    item1->setPos(posx,posy);
    timer_desitem->stop();
    timer_desitem->start(5000);
}

void jugar2player::desaparecer_items()
{
    if(item1!=nullptr){
        scene->removeItem(item1);
        item1->~items();
        item1=nullptr;
    }
}

void jugar2player::animar_item()
{
    if(item1!=nullptr){
        item1->actualizar(0.1);
        if (item1->get_y()<=500 && item1->collidesWithItem(l4)){
            item1->choquepiso();
        }
        item1->setPos(posxitem,item1->get_y());
        string punta1;
        QString npuntaje1;
        if(item1->collidesWithItem(jug1)){
            switch (item1->get_per()) {
            case 0:
                efecto=1;
                efectoporteria1=1;
                timer_efecto->stop();
                linea_gol1->~QGraphicsLineItem();
                scene->removeItem(linea_gol1);
                porteria1->~jugadorgraf();
                scene->removeItem(porteria1);
                linea_gol1=new QGraphicsLineItem(40,422,40,500);
                linea_gol1->setVisible(false);
                scene->addItem(linea_gol1);
                porteria1=new jugadorgraf(11);
                porteria1->setPos(0,380);
                scene->addItem(porteria1);
                bolajuego->setporterias(porteria1,porteria2);
                timer_efecto->start(10000);
                desaparecer_items();
                break;
            case 1:
                efecto=1;
                efectoporteria2=1;
                timer_efecto->stop();
                linea_gol2->~QGraphicsLineItem();
                scene->removeItem(linea_gol2);
                porteria2->~jugadorgraf();
                scene->removeItem(porteria2);
                linea_gol2=new QGraphicsLineItem(962,300,962,500);
                linea_gol2->setVisible(false);
                scene->addItem(linea_gol2);
                porteria2=new jugadorgraf(9);
                porteria2->setPos(920,240);
                scene->addItem(porteria2);
                bolajuego->setporterias(porteria1,porteria2);
                timer_efecto->start(10000);
                desaparecer_items();
                break;
            case 2:
                efectosal1=1;
                timer_efecto->stop();
                jug1->get_player()->set_vy(70);
                efecto=1;
                timer_efecto->start(10000);
                desaparecer_items();
                break;
            case 3:
                timer_efecto->stop();
                vel1=15;
                velchoque1=15;
                bolajuego->set_vxvy(velchoque1,velchoque2);
                efecto=1;
                timer_efecto->start(10000);;
                desaparecer_items();
                break;
            case 4:
                timer_efecto->stop();
                velypelota2=100;
                velxpelota2=50;
                vel2=2;
                efecto=1;
                timer_efecto->start(10000);
                desaparecer_items();
                break;
            case 5:
                timer_efecto->stop();
                velypelota1=700;
                velxpelota1=500;
                velypelota2=700;
                velxpelota2=500;
                efecto=1;
                timer_efecto->start(10000);
                desaparecer_items();
                break;
            case 6:
                puntaje1=puntaje1-1;
                if (puntaje1<=0)puntaje1=0;
                punta1=to_string(puntaje1);
                npuntaje1=QString::fromUtf8(punta1.c_str());
                //ui->label_4->setText(npuntaje1);
                ui->score1->display(npuntaje1);
                desaparecer_items();
                break;

            case 7:
                efecto=1;
                timer_efecto->stop();
                puntaje2=puntaje2-5;
                if (puntaje1<=0)puntaje1=0;
                efectosaltomal1=1;
                jug1->get_player()->set_vy(-10);
                timer_efecto->start(20000);
                punta1=to_string(puntaje1);
                npuntaje1=QString::fromUtf8(punta1.c_str());
                //ui->label_4->setText(npuntaje1);
                ui->score1->display(npuntaje1);
                desaparecer_items();
                break;
            }
        }
            else if(item1->collidesWithItem(jug2)){
                switch (item1->get_per()) {
                case 0:
                    efecto=1;
                    efectoporteria2=1;
                    timer_efecto->stop();
                    linea_gol2->~QGraphicsLineItem();
                    scene->removeItem(linea_gol2);
                    porteria2->~jugadorgraf();
                    scene->removeItem(porteria2);
                    linea_gol2=new QGraphicsLineItem(962,422,962,500);
                    linea_gol2->setVisible(false);
                    scene->addItem(linea_gol2);
                    porteria2=new jugadorgraf(12);
                    porteria2->setPos(920,380);
                    scene->addItem(porteria2);
                    bolajuego->setporterias(porteria1,porteria2);
                    timer_efecto->start(10000);
                    desaparecer_items();
                    break;

                case 1:
                    efecto=1;
                    efectoporteria1=1;
                    timer_efecto->stop();
                    linea_gol1->~QGraphicsLineItem();
                    scene->removeItem(linea_gol1);
                    porteria1->~jugadorgraf();
                    scene->removeItem(porteria1);
                    linea_gol1=new QGraphicsLineItem(40,300,40,500);
                    linea_gol1->setVisible(false);
                    scene->addItem(linea_gol1);
                    porteria1=new jugadorgraf(10);
                    porteria1->setPos(0,240);
                    scene->addItem(porteria1);
                    bolajuego->setporterias(porteria1,porteria2);
                    timer_efecto->start(10000);
                    desaparecer_items();
                    break;
                case 2:
                    efectosal2=1;
                    timer_efecto->stop();
                    jug2->get_player()->set_vy(70);
                    efecto=1;
                    timer_efecto->start(10000);
                    desaparecer_items();
                    break;
                case 3:
                    timer_efecto->stop();
                    vel2=15;
                    velchoque2=15;
                    bolajuego->set_vxvy(velchoque1,velchoque2);
                    efecto=1;
                    timer_efecto->start(10000);;
                    desaparecer_items();
                    break;
                case 4:
                    timer_efecto->stop();
                    velypelota1=100;
                    velxpelota1=50;
                    vel1=2;
                    efecto=1;
                    timer_efecto->start(10000);
                    desaparecer_items();
                    break;
                case 5:
                    timer_efecto->stop();
                    velypelota2=700;
                    velxpelota2=500;
                    velxpelota1=72;
                    velypelota1=250;
                    efecto=1;
                    timer_efecto->start(10000);
                    desaparecer_items();
                    break;
                case 6:
                    puntaje2=puntaje2-1;
                    if (puntaje2<=0)puntaje2=0;
                    punta1=to_string(puntaje2);
                    npuntaje1=QString::fromUtf8(punta1.c_str());
                    //ui->label_4->setText(npuntaje1);
                    ui->score2->display(npuntaje1);
                    desaparecer_items();
                    break;

                case 7:
                    efecto=1;
                    timer_efecto->stop();
                    puntaje2=puntaje2-5;
                    if (puntaje2<=0)puntaje2=0;
                    efectosaltomal2=1;
                    jug2->get_player()->set_vy(-10);
                    timer_efecto->start(20000);
                    punta1=to_string(puntaje2);
                    npuntaje1=QString::fromUtf8(punta1.c_str());
                    //ui->label_4->setText(npuntaje1);
                    ui->score2->display(npuntaje1);
                    desaparecer_items();
                    break;

                }
        }
    }

}

void jugar2player::reset_efecto()
{
    if(efecto==1){
            velchoque1=5;
            velchoque2=5;
            bolajuego->set_vxvy(velchoque1,velchoque2);
            efectosal1=0;
            efectosal2=0;
            efectosaltomal1=0;
            efectosaltomal2=0;
            vel1=5;
            vel2=5;
            velxpelota1=72;
            velypelota1=250;
            velxpelota2=72;
            velypelota2=250;
            efecto=0;
            if(efectoporteria1){
                linea_gol1->~QGraphicsLineItem();
                scene->removeItem(linea_gol1);
                porteria1->~jugadorgraf();
                scene->removeItem(porteria1);
                linea_gol1=new QGraphicsLineItem(40,370,40,500);
                linea_gol1->setVisible(false);
                scene->addItem(linea_gol1);
                porteria1=new jugadorgraf(7);
                porteria1->setPos(0,340);
                scene->addItem(porteria1);
                bolajuego->setporterias(porteria1,porteria2);
                efectoporteria1=0;
            }
            if(efectoporteria2){
                linea_gol2->~QGraphicsLineItem();
                scene->removeItem(linea_gol2);
                porteria2->~jugadorgraf();
                scene->removeItem(porteria2);
                linea_gol2=new QGraphicsLineItem(962,370,962,500);
                linea_gol2->setVisible(false);
                scene->addItem(linea_gol2);
                porteria2=new jugadorgraf(8);
                porteria2->setPos(920,340);
                scene->addItem(porteria2);
                bolajuego->setporterias(porteria1,porteria2);
                efectoporteria2=0;
            }
    }
}

jugar2player::~jugar2player()
{
    delete ui;
}


void jugar2player::on_pushButton_2_clicked()
{
    menu->pausar(timer_time,timerbola);
    menu->showNormal();
}
