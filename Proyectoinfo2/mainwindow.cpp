#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "single.h"
#include "playervsplayer.h"
#include <QMediaPlayer>
#include "mypushbuttom.h"
#include "pausemenu1.h"
#include <iostream>


MainWindow::MainWindow( bool a,pausemenu1 *menn,QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(0,0,1000,500);
    ui->view->setScene(scene);
    ui->view->setBackgroundBrush(QImage(":/imagenes/Fondo-estadio-futbol.jpg"));
    ui->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setFixedSize(1000,500);
    if(a){
        music=new pausemenu1(a);
    }
    else{
        music=menn;
    }
    connect(ui->pushButton_2,SIGNAL(Mouse_Posp()),this,SLOT(mouse_current_posp()));
    connect(ui->pushButton_2,SIGNAL(Mouse_Leftp()),this,SLOT(mouse_leavep()));
    connect(ui->pushButton,SIGNAL(Mouse_Posp()),this,SLOT(mouse_current_posp2()));
    connect(ui->pushButton,SIGNAL(Mouse_Leftp()),this,SLOT(mouse_leavep2()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_2_clicked()
{

    single *player= new single(music);
    player->show();
    close();


}

void MainWindow::on_pushButton_clicked()
{

    playervsplayer *twoplayer= new playervsplayer(music);
    twoplayer->show();
    close();
}

void MainWindow::on_actioncerrar_triggered()
{
    close();
}

void MainWindow::buttom1(bool actionn )//funcion para que aparesca una imagen si el mause esta en un area
{

       if(actionn) {QImage image;
        image.load(":/imagenes/Endo vol.png");
        ui->label_2->setPixmap(QPixmap::fromImage(image));
       }

    else{
        QImage image;
        image.load(":/imagenes/transparente.png");
        ui->label_2->setPixmap(QPixmap::fromImage(image));
       }
}

void MainWindow::buttom2(bool actionnn)//funcion para que aparesca una imagen si el mause esta en un area
{

    if(actionnn) {QImage image1,image2;
     image1.load(":/imagenes/Messi.png");
     image2.load(":/imagenes/Cristianovol.png");
     ui->label_3->setPixmap(QPixmap::fromImage(image1));
     ui->label_4->setPixmap(QPixmap::fromImage(image2));
    }

 else{
     QImage image1,image2;
     image1.load(":/imagenes/transparente.png");
     image2.load(":/imagenes/transparente.png");
     ui->label_3->setPixmap(QPixmap::fromImage(image1));
     ui->label_4->setPixmap(QPixmap::fromImage(image2));
    }
}

void MainWindow::mouse_current_posp()
{
    bool a=true;
    buttom1(a);
}

void MainWindow::mouse_leavep()
{
    bool b=false;
    buttom1(b);
}
void MainWindow::mouse_current_posp2()
{
    bool a=true;
    buttom2(a);
}

void MainWindow::mouse_leavep2()
{
    bool b=false;
    buttom2(b);
}







void MainWindow::on_actionpause_triggered()
{
    music->show();
}
