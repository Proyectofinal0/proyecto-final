#include "playervsplayer.h"
#include "seleccion_multiplayer.h"
#include "ui_playervsplayer.h"
#include "mainwindow.h"
#include <fstream>

playervsplayer::playervsplayer(pausemenu1 *pmenu,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::playervsplayer)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(0,0,1000,500);
    ui->view->setScene(scene);
    ui->view->setBackgroundBrush(QImage(":/imagenes/campo4-bicubic.jpg"));
    ui->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setFixedSize(1000,500);
    v_passwordp1=false;
    v_namep1=false;
    v_passwordp2=false;
    v_namep2=false;
    contador1=0;
    contador2=0;
    menu=pmenu;
    ui->pushButton_3->setVisible(false);

}

playervsplayer::~playervsplayer()
{
    delete ui;
}

void playervsplayer::on_pushButton_clicked()//boton de okey jugador 1, verifica si ya esta registrado y si no si la informacion ingresada es correcta
{
    QString nombre;
    if(contador1==0){
        nombre= ui->lineEditp1->text();
        name1= nombre.toStdString();
        if(name1!="" and name1.length()<12 and name1!=name2){
            ui->labelp1->setStyleSheet("color: white;");
            ui->labelp1->setText("Ingrese la contraseña: ");
        }
        else contador1=-1;
        if(name1.length()>=12){
            ui->labelp1->setStyleSheet("color: white;");
            ui->labelp1->setText("Ingrese un nombre mas corto: ");
        }
        if(name1==name2){
            ui->labelp1->setStyleSheet("color: white;");
            ui->labelp1->setText("Ingrese un nombre diferente: ");
        }
        ui->lineEditp1->clear();
    }
    else if(contador1==1){
        QString contrasena= ui->lineEditp1->text();
        passwordp1= contrasena.toStdString();
        contador1++;
    }
    if(contador1==2){
        ui->labelp1->setStyleSheet("color: white;");
        ui->labelp1->setText("Verificando...");
        verificar_usuario(name1,passwordp1,1);
        if(v_namep1){
            if(!v_passwordp1){
                contador1=0;
                //ui->pushButton->setText("Intenta otra vez");
                ui->labelp1->setStyleSheet("color: white;");
                ui->labelp1->setText("Contraseña incorrecta: ");
                ui->lineEditp1->clear();
            }
            else if(v_passwordp1){
                nombre=QString::fromUtf8(name1.c_str());;
                ui->nametag1->setStyleSheet("color: white;");
                ui->nametag1->setText(nombre);
                ui->welcome1->setStyleSheet("color: white;");
                ui->welcome1->setText("BIENVENIDO");
                ui->lineEditp1 ->close();
                ui->pushButton->close();
                ui->labelp1->close();
                ui->label->close();
                ui->pushButton_7->close();
                if(contador2==3){
                  ui->picture->setPixmap(QPixmap(":/imagenes/multijugadorimagen-1.png"));
                  ui->pushButton_3->setVisible(true);
                }
            }
        }
    }

    contador1++;
}

void playervsplayer::on_pushButton_2_clicked()//boton de okey jugador 2, verifica si ya esta registrado y si no si la informacion ingresada es correcta
{
    QString nombre2;
    if(contador2==0){
        nombre2= ui->lineEdit_2->text();
        name2= nombre2.toStdString();
        if(name2!="" and name2.length()<12 and name1!=name2){
            ui->labelp2->setStyleSheet("color: white;");
            ui->labelp2->setText("Ingrese la contraseña: ");
        }
        else contador2=-1;
        if(name2.length()>=12){
            ui->labelp2->setStyleSheet("color: white;");
            ui->labelp2->setText("Ingrese un nombre mas corto: ");
        }
        if(name1==name2){
            ui->labelp2->setStyleSheet("color: white;");
            ui->labelp2->setText("Ingrese un nombre diferente: ");
        }
        ui->lineEdit_2->clear();
    }
    else if(contador2==1){
        QString contrasena= ui->lineEdit_2->text();
        passwordp2= contrasena.toStdString();
        contador2++;
    }
    if(contador2==2){
        ui->labelp2->setStyleSheet("color: white;");
        ui->labelp2->setText("Verificando...");
        verificar_usuario(name2,passwordp2,2);
        if(v_namep2){
            if(!v_passwordp2){
                contador2=0;
                ui->labelp2->setStyleSheet("color: white;");
                ui->labelp2->setText("Contraseña incorrecta: ");
                ui->lineEdit_2->clear();
            }
            else if(v_passwordp2){
                nombre2=QString::fromUtf8(name2.c_str());;
                ui->nametag2->setStyleSheet("color: white;");
                ui->nametag2->setText(nombre2);
                ui->welcome2->setStyleSheet("color: white;");
                ui->welcome2->setText("BIENVENIDO");
                ui->lineEdit_2 ->close();
                ui->pushButton_2->close();
                ui->labelp2->close();
                ui->label->close();
                ui->pushButton_8->close();
                if(contador1==3){
                  ui->picture->setPixmap(QPixmap(":/imagenes/multijugadorimagen-1.png"));
                  ui->pushButton_3->setVisible(true);
                }
            }
        }
    }

    contador2++;
}

void playervsplayer::verificar_usuario(string nombre,string contra,int pl)
{
    //verificar si el usuario ya existia con anterioridar
    string nameverificar=nombre,passwordverificar=contra;
    bool estado=false,ingreso=false;
       ifstream archivonombres;
       char letra='k';
       string texto,linea;
       size_t poss=0;
       archivonombres.open("names.txt",ios::in);
       while(archivonombres.good()){
           getline(archivonombres,linea);
           texto="";
           poss=0;
           do{
               letra=linea[poss];
               texto+=letra;
               poss++;
           }while(linea[poss]!=':' and linea!="\0");
           if(texto==nombre){
               estado=true;
               break;
           }
           else estado=false;
       }
       //archivonombres.close();
       if(!estado){
           QString qnombre=QString::fromUtf8(nombre.c_str());;
           ofstream archivonombre;
           archivonombre.open("names.txt",ios::app);
           archivonombre<<nombre<<":"<<contra<<'\n';
           if(pl==1){
               ui->nametag1->setStyleSheet("color: white;");
               ui->nametag1->setText(qnombre);
               ui->lineEditp1->close();
               ui->pushButton->close();
               ui->labelp1->close();
               ui->welcome1->setStyleSheet("color: white;");
               ui->welcome1->setText("BIENVENIDO");
               ui->label->close();
               ui->pushButton_7->close();
               if(contador2==3){
                 ui->picture->setPixmap(QPixmap(":/imagenes/multijugadorimagen-1.png"));
                 ui->pushButton_3->setVisible(true);
               }

           }
           else if(pl==2){
               ui->nametag2->setStyleSheet("color: white;");
               ui->nametag2->setText(qnombre);
               ui->lineEdit_2->close();
               ui->pushButton_2->close();
               ui->labelp2->close();
               ui->welcome2->setStyleSheet("color: white;");
               ui->welcome2->setText("BIENVENIDO");
               ui->label->close();
               ui->pushButton_8->close();
               if(contador1==3){
                 ui->picture->setPixmap(QPixmap(":/imagenes/multijugadorimagen-1.png"));
                 ui->pushButton_3->setVisible(true);
               }

           }
       }
       else{
           string texto="";
               poss++;
               while(linea[poss]!='\0'){
                   texto+=linea[poss];
                   poss++;
               }
               if(texto==contra){
                   ingreso=true;
               }
               else {
                   ingreso=false;
               }

       }
       if(pl==1){
           v_passwordp1=ingreso;
           v_namep1=estado;
       }
       else if(pl==2){
           v_passwordp2=ingreso;
           v_namep2=estado;
       }
       ;

}



void playervsplayer::on_pushButton_3_clicked()//carga la informacion obtenida a la base de datos
//pasar a la seleccion de personaje
{
    //guardar informacion
    ui->picture->close();
    if(contador1==3 and contador2==3){
        bool flat1=false,flat2=false;
        string linea="",palabra="";
        size_t pos=0;
        ifstream verificardatosp1,verificardatosp2;
        verificardatosp1.open("datosmultiplayer.txt",ios::app);
        verificardatosp2.open("datosmultiplayer.txt",ios::app);
        while(verificardatosp1.good()){
            getline(verificardatosp1,linea);
            pos=0;
            palabra="";
            while(linea[pos]!='\0' and linea[pos]!=':' and !linea.empty()){
                palabra+=linea[pos];
                pos++;
            }
            if(palabra==name1){
                flat1=true;
                break;
            }
            else flat1=false;
        }
        linea="";
        pos=0;
        palabra="";
        while(verificardatosp2.good()){
            getline(verificardatosp2,linea);
            pos=0;
            palabra="";
            while(linea[pos]!='\0' and linea[pos]!=':' and !linea.empty()){
                palabra+=linea[pos];
                pos++;
            }
            if(palabra==name2){
                flat2=true;
                break;
            }
            else flat2=false;
        }
        if(!flat1){
            ofstream datos1;
            datos1.open("datosmultiplayer.txt",ios::app);
            datos1<<name1<<':'<<'0'<<','<<'0'<<','<<'0'<<';'<<"modo player vs player"<<'.'<<'\n';
        }
        if(!flat2){
            ofstream datos2;
            datos2.open("datosmultiplayer.txt",ios::app);
            datos2<<name2<<':'<<'0'<<','<<'0'<<','<<'0'<<';'<<"modo player vs player "<<'.'<<'\n';
        }
        ofstream enjuego;
        enjuego.open("nombresenjuego-pvsp.txt",ios::out);
        enjuego<<name1<<'\n'<<name2;
        enjuego.close();
        seleccion_multiplayer *selec= new seleccion_multiplayer(menu);
        selec->show();
        close();

    }
}

void playervsplayer::on_pushButton_5_clicked()
{
    bool music=false;
    MainWindow *man= new MainWindow(music);
    man->show();
    close();
}

void playervsplayer::on_pushButton_6_clicked()
{
    menu->showNormal();
}

void playervsplayer::on_pushButton_7_clicked()
{
    ui->lineEditp1->clear();
}

void playervsplayer::on_pushButton_8_clicked()
{
    ui->lineEdit_2->clear();
}
