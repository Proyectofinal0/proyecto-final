#include "seleccion_multiplayer.h"
#include "ui_seleccion_multiplayer.h"
#include "jugar2player.h"
#include "playervsplayer.h"
#include <map>
#include <fstream>

using namespace std;

seleccion_multiplayer::seleccion_multiplayer(pausemenu1 *pmenu,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::seleccion_multiplayer)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(0,0,1000,500);
    ui->view->setScene(scene);
    ui->view->setBackgroundBrush(QImage(":/imagenes/jugar1.jpg"));
    ui->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setFixedSize(1000,500);
    eleccionp1=0;
    eleccionp2=0;
    //cargar informacion de base de datos
    txtamap();
    string nam1,nam2;
    ifstream enjuegom;
    enjuegom.open("nombresenjuego-pvsp.txt",ios::in);
    while (enjuegom.good()) {
        getline(enjuegom,nam1);
        getline(enjuegom,nam2);
    }
    enjuegom.close();
    QString nombre1=QString::fromUtf8(nam1.c_str()),nombre2=QString::fromUtf8(nam2.c_str());
    ui->player1->setStyleSheet("color: white;");
    ui->player2->setStyleSheet("color: white;");
    ui->player1->setText(nombre1);
    ui->player2->setText(nombre2);
    menu=pmenu;
    QImage image1,image2;
    image1.load(":/imagenes/Cristianoselc.png");
    image2.load(":/imagenes/Cristianoselc.png");
    ui->personaje2->setPixmap(QPixmap::fromImage(image1));
    ui->personaje1->setPixmap(QPixmap::fromImage(image2));

}

seleccion_multiplayer::~seleccion_multiplayer()
{
    delete ui;
}

void seleccion_multiplayer::txtamap()
{
    //cargar datos de un txt
    string lineas,nombre="";
    size_t pos=0;
    ifstream archidatos;
    archidatos.open("datosmultiplayer.txt",ios::in);
    while (archidatos.good()) {
        getline(archidatos,lineas);
        pos=0;
        nombre="";
        while(lineas[pos]!='\0' and lineas[pos]!=':' and !lineas.empty()){
            nombre+=lineas[pos];
            pos++;
        }
        if(nombre!=""){
          datos[nombre]=lineas;
        }
    }
    archidatos.close();
}

void seleccion_multiplayer::maptotxt()
{
    //actualizar el archivo
    ofstream archidato;
    archidato.open("datosmultiplayer.txt",ios::out);
    map<string,string>::iterator it;
    for(it=datos.begin();it!=datos.end();it++){
        archidato<<datos[it->first]<<'\n';
    }
    archidato.close();
}

void seleccion_multiplayer::on_derch1_clicked()//cambia jugador para seleccionar j1
{
    if(eleccionp1<3) eleccionp1++;
    else eleccionp1=0 ;
    QImage image;
    switch (eleccionp1) {
    case 0:
        image.load(":/imagenes/Cristianoselc.png");
        ui->personaje1->setPixmap(QPixmap::fromImage(image));
        break;
    case 1:
        image.load(":/imagenes/Goenjiselec.png");
        ui->personaje1->setPixmap(QPixmap::fromImage(image));
        break;
    case 2:
        image.load(":/imagenes/messiselec.png");
        ui->personaje1->setPixmap(QPixmap::fromImage(image));
        break;
    case 3:
        image.load(":/imagenes/Endoselec.png");
        ui->personaje1->setPixmap(QPixmap::fromImage(image));
        break;

}

}

void seleccion_multiplayer::on_izq1_clicked()//cambia jugador para seleccionar j1
{
    if(eleccionp1>0) eleccionp1--;
    else eleccionp1=3 ;
    QImage image;
    switch (eleccionp1) {
    case 0:
        image.load(":/imagenes/Cristianoselc.png");
        ui->personaje1->setPixmap(QPixmap::fromImage(image));
        break;
    case 1:
        image.load(":/imagenes/Goenjiselec.png");
        ui->personaje1->setPixmap(QPixmap::fromImage(image));
        break;
    case 2:
        image.load(":/imagenes/messiselec.png");
        ui->personaje1->setPixmap(QPixmap::fromImage(image));
        break;
    case 3:
        image.load(":/imagenes/Endoselec.png");
        ui->personaje1->setPixmap(QPixmap::fromImage(image));
        break;

}

}

void seleccion_multiplayer::on_izq2_clicked()//cambia jugador para seleccionar j2
{
    QImage image;
    if(eleccionp2>0) eleccionp2--;
    else eleccionp2=3 ;

    switch (eleccionp2) {
    case 0:
        image.load(":/imagenes/Cristianoselc.png");
        ui->personaje2->setPixmap(QPixmap::fromImage(image));
        break;
    case 1:
        image.load(":/imagenes/Goenjiselec.png");
        ui->personaje2->setPixmap(QPixmap::fromImage(image));
        break;
    case 2:
        image.load(":/imagenes/messiselec.png");
        ui->personaje2->setPixmap(QPixmap::fromImage(image));
        break;
    case 3:
        image.load(":/imagenes/Endoselec.png");
        ui->personaje2->setPixmap(QPixmap::fromImage(image));
        break;

}

}

void seleccion_multiplayer::on_derch2_clicked()//cambia jugador para seleccionar j2
{
    QImage image;
    if(eleccionp2<3) eleccionp2++;
    else eleccionp2=0 ;
    switch (eleccionp2) {
    case 0:
        image.load(":/imagenes/Cristianoselc.png");
        ui->personaje2->setPixmap(QPixmap::fromImage(image));
        break;
    case 1:
        image.load(":/imagenes/Goenjiselec.png");
        ui->personaje2->setPixmap(QPixmap::fromImage(image));
        break;
    case 2:
        image.load(":/imagenes/messiselec.png");
        ui->personaje2->setPixmap(QPixmap::fromImage(image));
        break;
    case 3:
        image.load(":/imagenes/Endoselec.png");
        ui->personaje2->setPixmap(QPixmap::fromImage(image));
        break;
}

}

void seleccion_multiplayer::on_pushButton_clicked()//guarda informacion
{
    //guardar los personajes escogidos
    string linea1,linea2,palabra,nombre,aux1,aux2,name1,name2;
    size_t pos1=0,pos2=0;
    ifstream doc1,doc2,enjuegom;
    enjuegom.open("nombresenjuego-pvsp.txt",ios::in);
    while (enjuegom.good()) {
        getline(enjuegom,name1);
        getline(enjuegom,name2);
    }
    enjuegom.close();
    doc1.open("datosmultiplayer.txt",ios::in);
    while(doc1.good()){
        getline(doc1,linea1);
        pos1=0;
        nombre="";
        while(linea1[pos1]!='\0' and linea1[pos1]!=':' and !linea1.empty()){
            nombre+=linea1[pos1];
            pos1++;
        }
        if(nombre==name1) break;
    }
            doc1.close();
            doc2.open("datosmultiplayer.txt",ios::in);
            while(doc2.good()){
                getline(doc2,linea2);
                pos2=0;
                nombre="";
                while(linea2[pos2]!='\0' and linea2[pos2]!=':' and !linea2.empty()){
                    nombre+=linea2[pos2];
                    pos2++;
                }
                if(nombre==name2) break;
            }
                    doc2.close();
        while(linea1[pos1]!='\0' and linea1[pos1]!=',' and !linea1.empty()){
            aux1=to_string(eleccionp1);
            pos1++;
        }
        while(linea2[pos2]!='\0' and linea2[pos2]!=',' and !linea2.empty()){
            aux2=to_string(eleccionp2);
            pos2++;
        }
        pos1=pos1-1;
        linea1[pos1]=aux1[0];

        datos[name1]=linea1;
        pos2=pos2-1;
        linea2[pos2]=aux2[0];

        datos[name2]=linea2;
        maptotxt();

    jugar2player *twop=new jugar2player(menu);
    twop->show();
    close();
}


void seleccion_multiplayer::on_pushButton_3_clicked()
{
    playervsplayer *pltwo= new playervsplayer(menu);
    pltwo->show();
    close();
}

void seleccion_multiplayer::on_pushButton_4_clicked()
{
    menu->showNormal();
}
