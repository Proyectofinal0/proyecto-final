#ifndef SELECCION_MULTIPLAYER_H
#define SELECCION_MULTIPLAYER_H

#include <QWidget>
#include<map>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <pausemenu1.h>

using namespace std;
namespace Ui {
class seleccion_multiplayer;
}

class seleccion_multiplayer : public QWidget
{
    Q_OBJECT

public:
    explicit seleccion_multiplayer(pausemenu1 *pmenu=nullptr,QWidget *parent = nullptr);
    ~seleccion_multiplayer();
    void txtamap();
    void maptotxt();

private slots:
    void on_derch1_clicked();

    void on_izq1_clicked();

    void on_izq2_clicked();

    void on_derch2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::seleccion_multiplayer *ui;
    int eleccionp1;
    int eleccionp2;
    map<string,string>datos;
    QGraphicsView *view;
    QGraphicsScene *scene;
    pausemenu1   *menu;
};

#endif // SELECCION_MULTIPLAYER_H
