#-------------------------------------------------
#
# Project created by QtCreator 2018-11-22T17:12:07
#
#-------------------------------------------------
QT += gui
QT       += core gui serialport
QT       += core gui widgets
QT       +=multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Proyectoinfo2
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
CONFIG += resources_big


SOURCES += \
        main.cpp \
    mainwindow.cpp \
    single.cpp \
    playervsplayer.cpp \
    seleccion_personaje.cpp \
    seleccion_multiplayer.cpp \
    jugar1player.cpp \
    jugar2player.cpp \
    mypushbuttom.cpp \
    pausemenu1.cpp \
    jugador.cpp \
    jugadorgrafico.cpp \
    bola.cpp \
    items.cpp

HEADERS += \
        mainwindow.h \
    single.h \
    playervsplayer.h \
    seleccion_personaje.h \
    seleccion_multiplayer.h \
    jugar1player.h \
    jugar2player.h \
    mypushbuttom.h \
    pausemenu1.h \
    jugador.h \
    jugadorgrafico.h \
    bola.h \
    items.h

FORMS += \
        mainwindow.ui \
    single.ui \
    playervsplayer.ui \
    seleccion_personaje.ui \
    seleccion_multiplayer.ui \
    jugar1player.ui \
    jugar2player.ui \
    pausemenu1.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    archivos.qrc

DISTFILES +=
