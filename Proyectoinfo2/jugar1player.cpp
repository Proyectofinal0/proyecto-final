#include "jugar1player.h"
#include "mainwindow.h"
#include "ui_jugar1player.h"
#include <fstream>
#include <QTimer>
#include <QMessageBox>
#include "seleccion_personaje.h"
#include "bola.h"
#include <map>

using namespace std;
jugar1player::jugar1player(pausemenu1 *pmenu,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::jugar1player)
{
    ui->setupUi(this);
    txtamap();
    scene=new QGraphicsScene(0,0,1000,500);
    ui->view->setScene(scene);
    ui->view->setBackgroundBrush(QImage(":/imagenes/campo.jpg"));
    //relacionar un mapa con las funciones de las teclas
    on1=teclasmov[on1];
    on2=teclasmov[on2];
    on11=teclasmov[on11];
    on21=teclasmov[on21];
    salta2=1;
    salta=1;
    teclasmov[on11]=0;
    teclasmov[on21]=0;
    teclasmov[on1]=0;
    teclasmov[on2]=0;
    teclasmov[salta]=0;
    teclasmov[salta2]=0;
    //

    velitem3=false;
    flag2=true;
    max=0;
    efecto=0;
    numpar=0;
    puntaje=0;
    mins=0;
    empezar=0;
    efectosal=0;
    efectosalmal=0;
    segs1=0;
    segs2=0;
    velxpelota=100;
    velypelota=400;
    contador_linea=0;
    patear=false;
    patearhorizontal=false;
    menu=pmenu;
    timer_picture=new QTimer(this);
    timer_efecto=new QTimer(this);
    timer_desitem=new QTimer(this);
    timer_animaritem=new QTimer(this);
    timer_apitem=new QTimer(this);
    timer_teclas= new QTimer(this);
    timer_mov = new QTimer(this);
    timer_par = new QTimer(this);
    timer_lineas= new QTimer(this);
    timer_dificultad1= new QTimer(this);
    timer_dificultad2= new QTimer(this);
    timer_sal=new QTimer(this);
    timer_time=new QTimer(this);
    timer_control=new QTimer(this);
    control = new QSerialPort(this);
    timer_time->stop();
    timer_sal->stop();
    timer_lineas->stop();
    timer_dificultad1->stop();
    timer_dificultad2->stop();
    timer_mov->stop();
    timer_par->stop();
    timer_apitem->stop();
    timer_animaritem->stop();
    timer_desitem->stop();
    timer_efecto->stop();
    timer_control->stop();
    patear=false;
    mirder=true;
    mirizq=false;
    item1=nullptr;

    connect(timer_picture,SIGNAL(timeout()),this,SLOT(imagnefecto()));
    connect(timer_efecto,SIGNAL(timeout()),this,SLOT(reset_efecto()));
    connect(timer_desitem,SIGNAL(timeout()),this,SLOT(desaparecer_items()));
    connect(timer_animaritem,SIGNAL(timeout()),this,SLOT(animar_item()));
    connect(timer_apitem,SIGNAL(timeout()),this,SLOT(poner_items()));
    connect(timer_teclas,SIGNAL(timeout()),this,SLOT(moveteclas()));
    connect(timer_par,SIGNAL(timeout()),this,SLOT(moverlados()));
    connect(timer_lineas,SIGNAL(timeout()),this,SLOT(rotar_lineas()));
    connect(timer_dificultad1,SIGNAL(timeout()),this,SLOT(rotar_lineas()));
    connect(timer_dificultad2,SIGNAL(timeout()),this,SLOT(rotar_lineas()));
    connect(timer_sal,SIGNAL(timeout()),this,SLOT(saltar()));
    connect(timer_time,SIGNAL(timeout()),this,SLOT(time1()));
    connect(timer_control,SIGNAL(timeout()),this,SLOT(Joy()));

     timer_teclas->start(20);
     timer_lineas->start(15000);
     timer_apitem->start(15000);
     timer_animaritem->start(20);

    l1=new QGraphicsLineItem(0,0,1000,0);
    l2=new QGraphicsLineItem(0,0,0,500);
    l3=new QGraphicsLineItem(1000,0,1000,500);
    l4=new QGraphicsLineItem(0,500,1000,500,nullptr);

    diana=new jugadorgraf(5);
    diana->setPos(697,380);
    scene->addItem(diana);
    scene->addItem(l1);
    scene->addItem(l2);
    scene->addItem(l3);
    scene->addItem(l4);
    vel=5;
    i=0;

    timerbola = new QTimer;
    timerbola2 = new QTimer;
    timer_destruir=new QTimer;
    connect(timerbola,SIGNAL(timeout()),this,SLOT(animarbola()));
    connect(timerbola2,SIGNAL(timeout()),this,SLOT(aparecerbolas()));
    connect(timer_destruir,SIGNAL(timeout()),this,SLOT(destruir()));
    ui->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setFixedSize(1000,500);
    ui->lineEdit->setVisible(false);
    ui->label_7->setVisible(false);
    ui->pushButton->setVisible(false);
    ui->label_8->setVisible(false);

// cargar datos del jugador
    ifstream llenardata,namejuego;
    namejuego.open("nombreenjuegop1.txt",ios::in);
        string linea,nombregood,nameveri,texto;
        QString name,scoreq,partidosq;
        size_t pos=0;
    while (namejuego.good()) {
        getline(namejuego,nombregood);
    }
    namep=nombregood;
    name=QString::fromUtf8(nombregood.c_str());
    ui->nametag->setText(name);
    llenardata.open("datossingleplayer.txt",ios::in);
    while(llenardata.good()){
        getline(llenardata,linea);
        pos=0;
        texto="";
        nameveri="";
        while(linea[pos]!='\0' and linea[pos]!=':' and !linea.empty()){
            nameveri+=linea[pos];
            pos++;
        }
        pos++;
        if(nameveri==nombregood){
            while(linea[pos]!='\0' and linea[pos]!=',' and !linea.empty()){
                texto+=linea[pos];
                pos++;
            }
            personaje=atoi(texto.c_str());
            texto="";
            pos++;
            while(linea[pos]!='\0' and linea[pos]!=',' and !linea.empty()){
                texto+=linea[pos];
                pos++;
            }
            partidosq=QString::fromUtf8(texto.c_str());
            ui->numpartidos->setText(partidosq);
            numpar=atoi(texto.c_str());
            texto="";
            pos++;
            while(linea[pos]!='\0' and linea[pos]!=';' and !linea.empty()){
                texto+=linea[pos];
                pos++;
            }
            max=atoi(texto.c_str());
            scoreq=QString::fromUtf8(texto.c_str());
            ui->score->setText(scoreq);
        }
    }
    string punta=to_string(puntaje);
    QString npuntaje=QString::fromUtf8(punta.c_str());
    ui->label_4->display(npuntaje);

    jug=new jugadorgraf(personaje);
    jug->get_player()->set_valores(110,0,100,50);
    jug->posicion(500);
    scene->addItem(jug);
    scene->setFocusItem(jug);

    llenardata.close();

}

void jugar1player::inicio(bool w)//funcion que no deja que el juego empiece de una vez
{
    if(w and empezar==0){
        ui->empezar->close();
        ui->instrucciones->close();
        timer_time->start(1000);
        timerbola->start(20);
        timer_destruir->start(10000);
        timerbola2->start(4000);
        empezar=1;
    }
}


void jugar1player::keyPressEvent(QKeyEvent *event){       //Funciones de teclas
    if(event->key()==Qt::Key_F4) close();
    if(event->key()== Qt::Key_D && jug->get_player()->get_px()<930-(jug->get_player()->get_w()*jug->get_escala()))
    {
        mirder=true;
        mirizq=false;
        teclasmov[on1]=1;
        on1=teclasmov[on1];
        jug->eleccion_personaje(personaje);
        event->accept();
    }
   if(event->key()== Qt::Key_A&& jug->get_player()->get_px()>0)
    {
       mirder=false;
       mirizq=true;
       teclasmov[on2]=1;
       on2=teclasmov[on2];
       jug->eleccion_personaje_atras(personaje);
        event->accept();
    }
   if(event->key()==Qt::Key_H){
       patear=true;
   }
   if(event->key()==Qt::Key_J){
       patearhorizontal=true;
   }
    if(event->key()== Qt::Key_W )
    {
        teclasmov[salta]=0;
       salta=teclasmov[salta];
            event->accept();
    }
jug->posicion(500);
}

void jugar1player::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key()== Qt::Key_D &&jug->get_player()->get_px()>0)
    {
        teclasmov[on1]=0;
        on1=teclasmov[on1];
        event->accept();
    }
    if(event->key()== Qt::Key_A&& jug->get_player()->get_px()<930-(jug->get_player()->get_w()*jug->get_escala()))
     {
        teclasmov[on2]=0;
        on2=teclasmov[on2];
         event->accept();
     }
    if(event->key()== Qt::Key_W )
    {
        teclasmov[salta]=1;
        salta=teclasmov[salta];
            event->accept();
    }
    if(event->key()==Qt::Key_H){
        patear=false;
    }
    if(event->key()==Qt::Key_J){
        patearhorizontal=false;
    }

}

void jugar1player::moveteclas()//funcion que con los booleanos de un mapa realiza las acciones que esten activas
{
    action1(teclasmov[on2]);
    action2(teclasmov[on1]);
    action3(teclasmov[salta]);
    inicio(teclasmov[salta]);
}
void jugar1player::saltar(){
jug->saltar(500);
}

void jugar1player::action1(bool on)//derecha
{
    if(on && jug->get_player()->get_px()<940-(jug->get_player()->get_w()*jug->get_escala())){
        jug->get_player()->set_px(jug->get_player()->get_px()+vel);
    }
}
void jugar1player::action2(bool on)//izquierda
{
    if(on && jug->get_player()->get_px()>0){
        jug->get_player()->set_px(jug->get_player()->get_px()-vel) ;
    }
}

void jugar1player::action3(bool salta)//salta
{
    if(salta){
        timer_sal->stop();
        timer_sal->start(10);
        jug->get_player()->set_py(jug->get_player()->get_py());
        if (jug->get_player()->get_py()<=0 && jug->collidesWithItem(l4) ){
            if(!efectosal and !efectosalmal){//se define el tipo de choquepiso segun si tiene algun item o no
               jug->choquepiso(0);
            }
            else  if(efectosal)jug->choquepiso2(0);
            else  if(efectosalmal)jug->choquepiso3(0);
        }
    }
}
void jugar1player::moverlados()
{
if (i==0){
jug->get_player()->set_px(0);
jug->get_player()->set_py(200);
i=1;
}
if (jug->get_player()->get_py()>0) jug->moverlados(500);
}

void jugar1player::txtamap()
{
    string lineas,nombre="";
    size_t pos=0;
    ifstream archivodatos;
    archivodatos.open("datossingleplayer.txt",ios::in);
    while (archivodatos.good()) {
        getline(archivodatos,lineas);
        pos=0;
        nombre="";
        while(lineas[pos]!='\0' and lineas[pos]!=':' and !lineas.empty()){
            nombre+=lineas[pos];
            pos++;
        }
        if(nombre!=""){
         datos[nombre]=lineas;
        }

    }
    archivodatos.close();
}

void jugar1player::maptotxt()
{
    ofstream archidato;
    archidato.open("datossingleplayer.txt",ios::out);
    map<string,string>::iterator it;
    for(it=datos.begin();it!=datos.end();it++){
        if(it->second!=""){
             archidato<<datos[it->first]<<'\n';
        }
    }
    archidato.close();
}

void jugar1player::actualizarmap(string name)//actualizar informacion
{
    if(puntaje>max){
        max=puntaje;
        string newp=to_string(max),info="FELICIDADES, Nuevo puntaje maximo de  "+newp;
        QString mess=QString::fromUtf8(info.c_str());
        QMessageBox::information(this,"Nuevo record",mess);
    }
    numpar+=1;
    string numpersonaje=to_string(personaje),nummax=to_string(max),numpart=to_string(numpar);
    string newline=name+":"+numpersonaje+","+numpart+","+nummax+";"+"modo un jugador.";
    datos[name]=newline;
}
void jugar1player::aparecerbolas()
{

    bola * ball = new bola(jug);
    bolas.push_back(ball);
    scene->addItem(ball);
    scene->setFocusItem(ball);

}

void jugar1player::animarbola()
{
    for( int n= bolas.size(), i=n-1;i>=0;i--){
        bolas.at(i)->move(0.1,patear,personaje,mirder,mirizq,false,false,false,false,patearhorizontal,false,velypelota,velxpelota,false,false,velitem3);
        if(!(bolas.at(i)->collidingItems().empty()))
        {if(bolas.at(i)->collidesWithItem(diana)){
            scene->removeItem(bolas.at(i));
            bolas.at(i)->~bola();
            bolas.removeAt(i);
            puntaje+=1;
            string punta=to_string(puntaje);
            QString npuntaje=QString::fromUtf8(punta.c_str());
            ui->label_4->display(npuntaje);
        }}
    }
}

void jugar1player::destruir()
{
    if(!bolas.empty()){
        bolas.at(0)->~bola();
        bolas.pop_front();
    }
}
void jugar1player::poner_items()//aparencen items aleatorios y con posiciones aleatoreas
{
    int personaje,posy,posx;
    posy=10;
    //srand(time(NULL));
    posx=100+rand()%((100+800)-100);
    posxitem=posx;
    personaje=2+rand()%((2+7)-2);
    item1=new items(personaje);
    scene->addItem(item1);
    item1->setPos(posx,posy);
    timer_desitem->stop();
    timer_desitem->start(5000);
}

void jugar1player::desaparecer_items()
{
    if(item1!=nullptr){
        scene->removeItem(item1);
        item1->~items();
        item1=nullptr;
    }
}

void jugar1player::animar_item() //mueve el item y si choca con el jugador le otroga ciertas habilidades o le disminuye puntos
{
    if(item1!=nullptr){
        item1->actualizar(0.1);
        if (item1->get_y()<=500 && item1->collidesWithItem(l4)){
            item1->choquepiso();
        }
        item1->setPos(posxitem,item1->get_y());
        if(item1->collidesWithItem(jug)){
            string punta1;
            QString npuntaje1;
            switch (item1->get_per()) {//define segun el item el efecto al jugador
            case 2:
                efectosal=1;
                ui->itemss->setPixmap(QPixmap(":/imagenes/salto.png"));
                timer_picture->start(3000);
                timer_efecto->stop();
                jug->get_player()->set_vy(70);
                efecto=1;
                timer_efecto->start(10000);
                desaparecer_items();
                break;
            case 3:
                velitem3=true;
                timer_picture->start(3000);
                ui->itemss->setPixmap(QPixmap(":/imagenes/velocidad-3.png"));
                timer_efecto->stop();
                vel=15;
                efecto=1;
                timer_efecto->start(10000);;
                desaparecer_items();
                break;
            case 4:
                timer_efecto->stop();
                timer_picture->start(3000);
                ui->itemss->setPixmap(QPixmap(":/imagenes/slow-1.png"));
                velypelota=100;
                velxpelota=50;
                vel=2;
                efecto=1;
                timer_efecto->start(10000);
                desaparecer_items();
                break;
            case 5:
                timer_efecto->stop();
                timer_picture->start(3000);
                ui->itemss->setPixmap(QPixmap(":/imagenes/fuerza-1 (1).png"));
                velypelota=700;
                velxpelota=500;
                efecto=1;
                timer_efecto->start(10000);
                desaparecer_items();
                break;
            case 6:
                puntaje=puntaje-1;
                timer_picture->start(3000);
                ui->itemss->setPixmap(QPixmap(":/imagenes/Calavera-1.png"));
                if (puntaje<=0)puntaje=0;
                punta1=to_string(puntaje);
                npuntaje1=QString::fromUtf8(punta1.c_str());
                //ui->label_4->setText(npuntaje1);
                ui->label_4->display(npuntaje1);
                desaparecer_items();
                break;

            case 7:
                efecto=1;
                timer_picture->start(3000);
                ui->itemss->setPixmap(QPixmap(":/imagenes/Calavera-5.png"));
                timer_efecto->stop();
                puntaje=puntaje-5;
                if (puntaje<=0)puntaje=0;
                efectosalmal=1;
                jug->get_player()->set_vy(-10);
                timer_efecto->start(15000);
                punta1=to_string(puntaje);
                npuntaje1=QString::fromUtf8(punta1.c_str());
                //ui->label_4->setText(npuntaje1);
                ui->label_4->display(npuntaje1);
                desaparecer_items();
                break;
            }
        }
    }

}

void jugar1player::imagnefecto()//quita la imagen que pone el item
{
     ui->itemss->clear();
     timer_picture->stop();
}

void jugar1player::reset_efecto() //vuelve a los valores predeterminados
{
    if(efecto==1){
            velitem3=false;
            efectosal=0;
            efectosalmal=0;
            vel=5;
            velxpelota=100;
            velypelota=400;
            efecto=0;
    }
}
void jugar1player::rotar_lineas()//cambia de posicion los diferentes objetivos
{
    if(puntaje>=5 and puntaje<15){
        timer_lineas->stop();
        timer_dificultad1->start(8000);
        timer_dificultad2->stop();
    }
    else if(puntaje>=15){
        timer_lineas->stop();
        timer_dificultad1->stop();
        timer_dificultad2->start(5000);
    }
    switch (contador_linea) {
    case 0:
        scene->removeItem(diana);
        diana->~jugadorgraf();
        diana=new jugadorgraf(6);
        diana->setPos(600,300);
        scene->addItem(diana);
        break;
    case 1:
        scene->removeItem(diana);
        diana->~jugadorgraf();
        diana=new jugadorgraf(4);
        diana->setPos(100,200);
        scene->addItem(diana);
        break;
    case 2:
        scene->removeItem(diana);
        diana->~jugadorgraf();
        diana=new jugadorgraf(5);
        diana->setPos(300,300);
        scene->addItem(diana);
        break;
    case 3:
        scene->removeItem(diana);
        diana->~jugadorgraf();
        diana=new jugadorgraf(5);
        diana->setPos(800,250);
        scene->addItem(diana);
        break;
    case 4:
        scene->removeItem(diana);
        diana->~jugadorgraf();
        diana=new jugadorgraf(5);
        diana->setPos(700,150);
        scene->addItem(diana);
        break;
    case 5:
        scene->removeItem(diana);
        diana->~jugadorgraf();
        diana=new jugadorgraf(6);
        diana->setPos(100,200);
        scene->addItem(diana);
        break;
    case 6:
        scene->removeItem(diana);
        diana->~jugadorgraf();
        diana=new jugadorgraf(6);
        diana->setPos(400,250);
        scene->addItem(diana);
        break;
    case 7:
        scene->removeItem(diana);
        diana->~jugadorgraf();
        diana=new jugadorgraf(5);
        diana->setPos(750,350);
        scene->addItem(diana);
        break;
    }
    if(contador_linea<7)contador_linea+=1;
    else contador_linea=0;

}

void jugar1player::on_pushButton_2_clicked()
{
    menu->pausar(timer_time,timerbola2,timer_efecto);
    menu->showNormal();
}
void jugar1player::on_pushButton_3_clicked()
{
    ui->lineEdit->setVisible(true);
    ui->label_7->setVisible(true);
    ui->pushButton->setVisible(true);

}

void jugar1player::time1() //funcion que simula un reloj y ayuda a saber el resultado final al terminar el juego
{
    segs2+=1;
    if(segs2==10){
        segs1+=1;
        segs2=0;
        if(segs1==6) {
            mins+=1;
            segs1=0;
        }
    }
    string mns=to_string(mins),sgs1=to_string(segs1),sgs2=to_string(segs2),punta=to_string(puntaje);
    const string text="Fin del juego,tu puntaje fue de  "+punta;
    QString mn=QString::fromUtf8(mns.c_str()),sg1=QString::fromUtf8(sgs1.c_str()),sg2=QString::fromUtf8(sgs2.c_str()),mensaje=QString::fromUtf8(text.c_str());
    ui->seg->setText(sg1);
    ui->seg_2->setText(sg2);
    ui->min->setText(mn);
    if(mins==2){
        //flag=false;
        timer_time->stop();
        timer_sal->stop();
        timer_lineas->stop();
        timer_dificultad1->stop();
        timer_dificultad2->stop();
        timer_mov->stop();
        timer_par->stop();
        timer_apitem->stop();
        timer_animaritem->stop();
        timer_desitem->stop();
        timer_efecto->stop();
        //timer_control->stop();
        timerbola->stop();
        timerbola2->stop();
        QMessageBox::information(this,"Fin del juego",mensaje);
        while (!flag2) {
              QMessageBox::warning(this,"Mando","Oprime F (En el mando) para desconectar el mando");//mensaje que aparece unicamente si el mando esta conectado
        }
        QMessageBox::StandardButton ok=QMessageBox::question(this,"Fin del juego","¿Quieres volver a jugar?",QMessageBox::Yes | QMessageBox::No);
        if(ok==QMessageBox::Yes){
            actualizarmap(namep);
            maptotxt();
            seleccion_personaje *win=new seleccion_personaje(menu);
            win->showNormal();
            close();
        }
        else if(ok==QMessageBox::No){
            actualizarmap(namep);
            maptotxt();
            bool music=false;
            MainWindow *win1=new MainWindow(music,menu);
            win1->show();
            close();

        }
    }
}


void jugar1player::Joy(){
        char data;
        int l = 0;
        flag2=false;
        bool flag=true;
        while(flag){
              if(control->waitForReadyRead(100)){

                //Data was returned
                l = control->read(&data,1);
                switch (data) {
                case 'A':
                    teclasmov[salta2]=1;
                    if (jug->get_player()->get_py()<=0 && jug->collidesWithItem(l4) ){
                           jug->choquepiso(0);
                        }
                    break;
                case 'B':
                    mirder=true;
                    mirizq=false;
                    teclasmov[on11]=1;
                    on1=teclasmov[on11];
                    jug->eleccion_personaje(personaje);
                    break;
                case 'C':
                    patear=true;
                    break;
                case 'D':
                    mirder=false;
                    mirizq=true;
                    teclasmov[on21]=1;
                    on2=teclasmov[on21];
                    jug->eleccion_personaje_atras(personaje);
                    break;
                case 'E':
                    patearhorizontal=true;
                    break;
                case'F':
                    flag2=true;
                    flag=false;
                    timer_control->stop();
                    control->close();
                    break;
                case 'K':
                    patearhorizontal=true;
                    break;
                case 'N':
                    teclasmov[on11]=0;
                    on1=teclasmov[on11];
                    teclasmov[on21]=0;
                    on2=teclasmov[on21];
                    patear=false;
                    patearhorizontal=false;
                    teclasmov[salta2]=0;
                    salta2=teclasmov[salta2];
                    flag=false;
                    break;

                default:
                    flag=false;
                    break;
                }
                if(flag)
                jug->posicion(500);
                qDebug()<<"Response: "<<data;
                flag=false;

            }else{
                //No data
                qDebug()<<"Time out";
                flag2=true;
                flag=false;
                timer_control->stop();
                control->close();
          }
        }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

}

void jugar1player::on_pushButton_clicked()
{
    QString ingresado;
    string comprobar;
    ingresado= ui->lineEdit->text();
    puerto= "COM"+ingresado;
    comprobar=ingresado.toStdString();
    ui->label_7->close();
    if(comprobar <="9" &&  comprobar>="0"){
    control->setPortName( puerto);
    if(control->open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!control->setBaudRate(QSerialPort::Baud9600)) //Configurar la tasa de baudios
            qDebug()<<control->errorString();

        if(!control->setDataBits(QSerialPort::Data8))
            qDebug()<<control->errorString();

        if(!control->setParity(QSerialPort::NoParity))
            qDebug()<<control->errorString();

        if(!control->setStopBits(QSerialPort::OneStop))
            qDebug() <<control->errorString();

        if(!control->setFlowControl(QSerialPort::NoFlowControl))
            qDebug()<<control->errorString();
            timer_control->start(20);
    }
    else{
        qDebug()<<"Serial COM"+ingresado+ "not opened. Error: "<<control->errorString();
    }
    ui->lineEdit->close();
    ui->pushButton->close();
    }
    else{
        ui->label_8->setVisible(true);
        ui->lineEdit->clear();
     }
}

jugar1player::~jugar1player()
{
    delete ui;
    delete timerbola;
    delete timerbola2;
    delete  timer_animaritem;
    delete  timer_apitem;
    delete  timer_desitem;
    delete  timer_destruir;
    delete  timer_dificultad1;
    delete  timer_dificultad2;
    delete timer_efecto;
    delete timer_lineas;
    delete timer_mov;
    delete timer_par;
    delete timer_picture;
    delete  timer_sal;
    delete timer_teclas;
    delete timer_time;
}




