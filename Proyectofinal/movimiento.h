#ifndef MOVIMIENTO_H
#define MOVIMIENTO_H
#include <QGraphicsItem>
#include <QPainter>

class movimiento: public QGraphicsItem
{
private:
    double  Masa;
    double  Px;
    double  Py;
    double  Vx;
    double  Vy;
    double constG;
    double Cofriccion;
    double Radio;
    double e;
    double V;
public:
    movimiento();
    QRectF boundingRect() const;
    void paint(QPainter *painter,
    const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mover();
    void choquex();
    void choquey();
    void actualizar(double dt=0.1);
};

#endif // MOVIMIENTO_H
