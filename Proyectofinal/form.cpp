#include "form.h"
#include "ui_form.h"
#include <QMessageBox>
#include "movimiento.h"


Form::Form(QWidget *parent) :
    QMainWindow (parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
    scene2=new QGraphicsScene(0,0,1000,500);
    ui->graphicsView->setScene(scene2);


    ui->graphicsView->setBackgroundBrush(QImage(":/Imagenesjuego/campo.jpg"));
}

Form::~Form()
{
    delete ui;
}

void Form::animar()
{

    for(int i=0; i<bolas.length();i++)
    {

        if (!bolas.at(i)->collidingItems().empty()){

            bolas.at(i)->choquex();
            if (bolas.at(i)->collidesWithItem(l2)|| bolas.at(i)->collidesWithItem(l3)){
                bolas.at(i)->choquey();
            }
    }
        bolas.at(i)->mover();
    }
}
void Form::mousePressEvent(QMouseEvent *ev)
{
    bolas.append(new movimiento());
    scene2->addItem(bolas.last());
    bolas.last()->setPos(ev->x(),ev->y());
}
