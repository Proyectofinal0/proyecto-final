#include "movimiento.h"


movimiento::movimiento()
{
    Vy=7;
    Masa=70;
    Px=x();
    Py=y();
    Vx=7;
    constG=10;
    Cofriccion=0.1;
    Radio=10;
    e=0.7;
    V=0;

    }
QRectF movimiento::boundingRect() const
{
    return QRectF(-10,-10,Radio*2,Radio*2);
}

void movimiento::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPixmap pixmap;
    pixmap.load(":/Imagenesjuego/balon.png");
    painter->drawPixmap(boundingRect(),pixmap,pixmap.rect());
}

void movimiento::actualizar(double dt){
        double angulo=atan2(Vy,Vx);
        V=sqrt(pow(Vx,2)+pow(Vy,2));
        double ax=-Cofriccion*pow(V,2)*pow(Radio,2)/Masa*cos(angulo);
        double ay=-Cofriccion*pow(V,2)*pow(Radio,2)/Masa*sin(angulo)+constG;
        Px=Px+Vx*dt;
        Py=Py+Vy*dt+constG*dt*dt/2;
        Vy=Vy+ay*dt;
        Vx=Vx+ax*dt;

    }

void movimiento::mover()
    {
        setPos(x()+Px,y()+Py);
        actualizar();

    }

void movimiento::choquex()
    {
        Py=-(Py*e);

    }
void movimiento::choquey(){
        Px=-(Px*e);

    }



