#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsLineItem>
#include <QKeyEvent>
#include <QTimer>
#include <movimiento.h>

namespace Ui {
class Form;
}

class Form : public QMainWindow
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = nullptr);
    ~Form();
signals:
    void aviso(int);
public slots:
    void animar();

private:
    Ui::Form *ui;
    QGraphicsScene* scene2;
    QGraphicsLineItem* l1;
    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;
    bool flag=true;
    QList<movimiento*> bolas;

    QTimer *timer;

    void mousePressEvent(QMouseEvent *ev);

};

#endif // FORM_H
