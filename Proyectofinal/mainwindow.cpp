#include "mainwindow.h"
#include "ui_mainwindow.h"
#include  <QMediaPlayer>
#include "form.h"
#include <QWidget>
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QMediaPlayer *music=new QMediaPlayer();
    music->setMedia(QUrl("qrc:/Imagenesjuego/live-it-up-official-video-nicky-jam-feat-will-smith-era-istrefi-2018-fifa-world-cup-russia.mp3"));
    music->play();
    scene=new QGraphicsScene(0,0,1000,500);
    ui->graphicsView->setScene(scene);
     ui->graphicsView->setBackgroundBrush(QImage(":/Imagenesjuego/Fondo-estadio-futbol.jpg"));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionCerrar_triggered()
{
    QApplication::quit();
}

void MainWindow::on_pushButton_clicked()
{
    close();
    Form *mostrar = new Form(); mostrar->show();
}

void MainWindow::on_pushButton_2_clicked()
{
    close();
    Form *mostrar = new Form(); mostrar->show();
}
